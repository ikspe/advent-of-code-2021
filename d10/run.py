import argparse
import typing


ALLOWED_CHARS_AND_SCORE = {'{}': 1197, '()': 3, '[]': 57, '<>': 25137}
CLOSING_CHARS_AND_SCORE = {x[1]: v for x, v in ALLOWED_CHARS_AND_SCORE.items()}
OPENING_CHARS = {x[0] for x in ALLOWED_CHARS_AND_SCORE}
CLOSING_TO_OPENING_CHARS = {x[1]: x[0] for x in ALLOWED_CHARS_AND_SCORE}
OPENING_TO_CLOSING_CHARS = {x[0]: x[1] for x in ALLOWED_CHARS_AND_SCORE}
AUTOCOMPLETION_SCORE = {')': 1, ']': 2, '}': 3, '>': 4}


def compute_illegal_score(line: str) -> typing.Tuple[bool, int, str]:
    stack_chunks = []
    for c in line:
        if c in CLOSING_CHARS_AND_SCORE:
            if not stack_chunks:
                return True, CLOSING_CHARS_AND_SCORE[c], c
            if stack_chunks[-1] != CLOSING_TO_OPENING_CHARS[c]:
                return True, CLOSING_CHARS_AND_SCORE[c], c
            stack_chunks.pop()
            continue
        assert c in OPENING_CHARS, f"{c} not in {OPENING_CHARS}"
        stack_chunks.append(c)

    # finished without corruption
    missing_chars = [OPENING_TO_CLOSING_CHARS[x] for x in stack_chunks]
    missing_chars.reverse()
    autocompletion_score = 0
    for c in missing_chars:
        autocompletion_score = autocompletion_score * 5 + AUTOCOMPLETION_SCORE[c]
    return False, autocompletion_score, "".join(missing_chars)


def main(is_part1: bool, input_file: str, verbose_level=0):

    list_all_scores = []

    with open(input_file) as f:
        for line in f:
            line = line.strip()
            is_corrupted, line_score, explanation = compute_illegal_score(line)
            if is_corrupted:
                if is_part1 and verbose_level >= 1 or not is_part1 and verbose_level >= 2:
                    print(f"[DEBUG] corrupted line (score =  {line_score:8d}): {line} ... corrupted char = {explanation}")
                if is_part1:
                    list_all_scores.append(line_score)
            else:
                if is_part1 and verbose_level >= 2 or not is_part1 and verbose_level >= 1:
                    print(f"[DEBUG] incomplete line (score = {line_score:8d}): {line} ... missing = {explanation}")
                if not is_part1:
                    list_all_scores.append(line_score)
            continue

    if is_part1:
        print(f"Result (total syntax error score) = {sum(list_all_scores)}")
    else:
        list_all_scores.sort()
        count_scores = len(list_all_scores)
        idx_middle = (count_scores - 1) / 2
        if idx_middle == int(idx_middle):
            idx_middle = int(idx_middle)
            print(f"Result (middle score of autocompletion) = {list_all_scores[idx_middle]}   (idx = {idx_middle} / count_scores = {count_scores})")
        else:
            print(f"Warning: idx_middle = {idx_middle} / count_scores = {count_scores}")
            for idx_middle in [int(idx_middle), int(idx_middle) + 1]:
                print(f"Possible result: (middle score of autocompletion) = {list_all_scores[idx_middle]}   (idx = {idx_middle} / count_scores = {count_scores})")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
