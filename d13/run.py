import argparse
import typing


class Origami2:
    _debug_instance = None

    def __init__(self, verbose_level=0):
        self._grid: typing.Dict[typing.Tuple[int, int], str] = {}
        self.verbose_level = verbose_level
        Origami2._debug_instance = self

    def add_instruction(self, instruction: str) -> bool:
        if not instruction:
            return False
        fold = instruction.split("fold along ")
        if len(fold) > 1:
            assert len(fold) == 2
            fold = fold[1]
            axis, pivot = fold.split("=")
            assert axis in ("x", "y")
            pivot = int(pivot)
            if axis == "y":
                # fold up
                for k, v in self._grid.copy().items():
                    if k[1] > pivot:
                        del self._grid[k]
                        self._grid[(k[0], 2 * pivot - k[1])] = v
            else:
                # fold left
                for k, v in self._grid.copy().items():
                    if k[0] > pivot:
                        del self._grid[k]
                        self._grid[(2 * pivot - k[0], k[1])] = v
            return True
        else:
            x, y = instruction.split(",")
            x, y = int(x), int(y)
            self._grid[(x, y)] = "#"
            return False

    def draw(self):
        grid_to_print = [["."]]
        for (x, y), v in self._grid.items():
            if y >= len(grid_to_print):
                for _ in range(y + 1 - len(grid_to_print)):
                    grid_to_print.append(['.'] * len(grid_to_print[0]))
            if x >= len(grid_to_print[0]):
                missing_cols = x + 1 - len(grid_to_print[0])
                for line in grid_to_print:
                    line.extend(['.'] * missing_cols)
            grid_to_print[y][x] = v
        for line in grid_to_print:
            print("".join(line))
        pass

    def count_dots(self):
        return len(self._grid)


def main(is_part1: bool, input_file: str, verbose_level=0):
    origami = Origami2(verbose_level=verbose_level)
    with open(input_file) as f:
        for line in f:
            was_fold = origami.add_instruction(line.strip())
            if verbose_level >= 3:
                print(f"Origami after instruction {line.strip()}:")
                origami.draw()
            if is_part1 and was_fold:
                break
    if verbose_level >= 1:
        print(f"Origami final state:")
        origami.draw()
    if is_part1:
        count_dots = origami.count_dots()
        print(f"Result (number of visible dots) = {count_dots}")
    else:
        origami.draw()


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
