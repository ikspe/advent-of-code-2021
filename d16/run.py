import argparse
import typing


class Packet:
    def __init__(self, version: int, type_id: int):
        self._version = version
        self._type_id = type_id

    def get_version_sum(self) -> int:
        raise NotImplementedError

    def get_value(self) -> int:
        raise NotImplementedError


class PacketLiteralValue(Packet):
    def __init__(self, version: int, type_id: int, value: int):
        Packet.__init__(self, version, type_id)
        self._value = value

    def get_version_sum(self) -> int:
        return self._version

    def get_value(self) -> int:
        return self._value

    def __str__(self):
        # return f"PacketLiteralValue<v={self._version}/t={self._type_id}/value={self._value}>"
        return f"{self._value}"

    def __repr__(self):
        return str(self)


class PacketOperator(Packet):
    _OPERATORS_STR = {0: "+", 1: "*", 2: "min", 3: "max", 5: ">", 6: "<", 7: "=="}

    def __init__(self, version: int, type_id: int, sub_packets: typing.List[Packet]):
        Packet.__init__(self, version, type_id)
        self._sub_packets = sub_packets

    def get_version_sum(self) -> int:
        return self._version + sum([x.get_version_sum() for x in self._sub_packets])

    def get_value(self) -> int:
        if self._type_id == 0:
            return sum([x.get_value() for x in self._sub_packets])
        elif self._type_id == 1:
            res = 1
            for x in self._sub_packets:
                res *= x.get_value()
            return res
        elif self._type_id == 2:
            return min([x.get_value() for x in self._sub_packets])
        elif self._type_id == 3:
            return max([x.get_value() for x in self._sub_packets])
        elif self._type_id == 5:
            assert len(self._sub_packets) == 2
            return int(self._sub_packets[0].get_value() > self._sub_packets[1].get_value())
        elif self._type_id == 6:
            assert len(self._sub_packets) == 2
            return int(self._sub_packets[0].get_value() < self._sub_packets[1].get_value())
        elif self._type_id == 7:
            assert len(self._sub_packets) == 2
            return int(self._sub_packets[0].get_value() == self._sub_packets[1].get_value())
        raise NotImplementedError

    def __str__(self):
        # return f"PacketOperator<v={self._version}/t={self._type_id}/packets={self._sub_packets}>"
        return f"{self._OPERATORS_STR[self._type_id]} {self._sub_packets}"

    def __repr__(self):
        return str(self)


class PacketDecoder:
    def __init__(self, verbose_level=0):
        self.verbose_level = verbose_level

    @staticmethod
    def bits_to_value(bits_list: list) -> int:
        res = 0
        for v in bits_list:
            res = res * 2 + v
        return res

    def _decode_from_pos(self, data: typing.List[int], pos) ->\
            typing.Optional[typing.Tuple[Packet, int]]:
        if self.verbose_level >= 2:
            print(f"[DEBUG] decode at pos = {pos} ; len(data) = {len(data)}")
        if len(data) < pos + 6:
            return None
        version = self.bits_to_value(data[pos:pos+3])
        type_id = self.bits_to_value(data[pos + 3:pos+6])
        pos += 6
        if len(data) == pos and version == 0 and type_id == 0:
            return None
        if self.verbose_level >= 2:
            print(f"[DEBUG]     found version = {version} / type_id = {type_id}")
        if type_id == 4:
            # literal value
            value = 0
            while True:
                bits_list = data[pos:pos+5]
                pos += 5
                value = value * 16 + self.bits_to_value(bits_list[1:])
                if bits_list[0] == 0:
                    break
            if self.verbose_level >= 2:
                print(f"[DEBUG] finished literal value = {value} ; pos = {pos}")
            packet = PacketLiteralValue(version, type_id, value)
        else:
            sub_packets: typing.List[Packet] = []
            length_type = data[pos]
            pos += 1
            if length_type == 0:
                total_length_sub_packet = self.bits_to_value(data[pos:pos+15])
                pos += 15
                sub_pos = 0
                sub_data = data[pos:pos+total_length_sub_packet]
                if self.verbose_level >= 2:
                    print(f"[DEBUG]     operator with length_type = {length_type} / total_length_sub_packet = {total_length_sub_packet}")
                while sub_pos < total_length_sub_packet:
                    if self.verbose_level >= 2:
                        print(f"[DEBUG]     operator with length_type = {length_type} / pos = {pos} / sub_pos = {sub_pos} / total_length_sub_packet = {total_length_sub_packet}")
                    res = self._decode_from_pos(sub_data, sub_pos)
                    sub_packets.append(res[0])
                    sub_pos = res[1]
                    if self.verbose_level >= 2:
                        print(f"[DEBUG]     operator with length_type = {length_type} / pos = {pos} / finished sub_pos = {sub_pos} / total_length_sub_packet = {total_length_sub_packet}")
                pos += sub_pos
            else:
                sub_packets_count = self.bits_to_value(data[pos:pos+11])
                pos += 11
                if self.verbose_level >= 2:
                    print(f"[DEBUG]     operator with length_type = {length_type} / sub_packets_count = {sub_packets_count}")
                for _ in range(sub_packets_count):
                    res = self._decode_from_pos(data, pos)
                    sub_packets.append(res[0])
                    pos = res[1]
            if self.verbose_level >= 2:
                print(f"[DEBUG] finished operator with len(sub_packets) {len(sub_packets)}")
            packet = PacketOperator(version, type_id, sub_packets)
        return packet, pos

    def decode(self, data_str: str) -> typing.List[Packet]:
        if self.verbose_level >= 1:
            print(f"[DEBUG] decode() data_str = {data_str}")
        data: typing.List[int] = []
        for d in data_str:
            data += [(int(d, 16) >> i) % 2 for i in range(3, -1, -1)]
        if self.verbose_level >= 2:
            print(f"[DEBUG] decode() len(data) = {len(data)} / data = {data}")
        packets_list: typing.List[Packet] = []
        pos = 0
        while True:
            res = self._decode_from_pos(data, pos)
            if res is None:
                if self.verbose_level >= 2:
                    print(f"[DEBUG] found end of data at pos = {pos} ; len(data) = {len(data)}")
                assert all([x == 0 for x in data[pos:]])
                break
            packets_list.append(res[0])
            pos = res[1]
        return packets_list


def main(is_part1: bool, input_file: str, verbose_level=0):
    decoder = PacketDecoder(verbose_level=verbose_level)
    with open(input_file) as f:
        data_str = f.readline().strip()
    decoded_values = decoder.decode(data_str)
    if verbose_level >= 1:
        print(f"Got values = {decoded_values}")
    if is_part1:
        res = sum([x.get_version_sum() for x in decoded_values])
        if verbose_level >= 1:
            print(f"Result part 1: {res}")
    else:
        assert len(decoded_values) == 1
        res = decoded_values[0].get_value()
        if verbose_level >= 1:
            print(f"Result part 2: {res}")
    return res


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    import time
    time_begin = time.time()
    result = main(args.part1, args.input, verbose_level=args.verbose)
    time_end = time.time()
    print(f"Elapsed time: {time_end - time_begin}")
    print(f"Result: {result}")
