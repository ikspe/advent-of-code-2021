import argparse
import math
import typing


class SnailFishValue:

    @classmethod
    def decode(cls, line: str):
        val, line = cls._decode_line_at_pos(line)
        assert len(line) == 0
        return val

    @classmethod
    def _decode_line_at_pos(cls, line: str):
        assert line[0] not in ("]", ",")
        if line[0] == "[":
            line = line[1:]
            first_token, line = cls._decode_line_at_pos(line)
            assert line[0] == ","
            line = line[1:]
            second_token, line = cls._decode_line_at_pos(line)
            assert line[0] == "]"
            line = line[1:]
            return PairValue(first_token, second_token), line
        else:
            # go until next "]" or ","
            list_pos_end_tokens = [t2 for t2 in [line.find(t) for t in ["]", ","]] if t2 != -1]
            assert list_pos_end_tokens
            pos_end_token = min(list_pos_end_tokens)
            res = IntValue(int(line[0:pos_end_token]))
            line = line[pos_end_token:]
            return res, line

    def __add__(self, other):
        """operator +="""
        res: SnailFishValue = PairValue(self, other)
        res.reduce()
        return res

    def reduce(self):
        while True:
            if self._try_explode([self]):
                continue
            if self._try_split():
                continue
            break

    def _try_explode(self, levels) -> bool:
        raise NotImplementedError

    def _should_split(self):
        raise NotImplementedError

    def _try_split(self):
        raise NotImplementedError

    @property
    def magnitude(self) -> int:
        raise NotImplementedError

    def copy(self):
        raise NotImplementedError


class IntValue(SnailFishValue):
    def __init__(self, val: int):
        self._value = val

    def __str__(self):
        return f"{self._value}"

    def __repr__(self):
        return f"{type(self).__name__}<{self._value!r}>"

    def __eq__(self, other):
        other: IntValue = other
        return isinstance(other, IntValue) and self._value == other._value

    def _try_explode(self, levels: typing.List[SnailFishValue]) -> bool:
        return False

    def _should_split(self) -> bool:
        return self._value >= 10

    def _try_split(self):
        return PairValue(IntValue(self._value // 2), IntValue(math.ceil(self._value/2)))

    @property
    def magnitude(self) -> int:
        return self._value

    def copy(self):
        return IntValue(self._value)


class PairValue(SnailFishValue):
    def __init__(self, left: SnailFishValue, right: SnailFishValue):
        self._left = left
        self._right = right

    def __str__(self):
        return f"[{self._left},{self._right}]"

    def __repr__(self):
        return f"{type(self).__name__}<[{self._left!r},{self._right!r}]>"

    def __eq__(self, other):
        other: PairValue = other
        return isinstance(other, PairValue) and self._left == other._left and self._right == other._right

    def _try_explode(self, levels: typing.List[SnailFishValue]) -> bool:
        # YES, I KNOW, THIS IS UGLY... BUT WORKING ! :-(
        assert len(levels) <= 5
        if len(levels) == 5:
            levels.pop()
            assert isinstance(self._left, IntValue)
            assert isinstance(self._right, IntValue)
            elem_for_left = self._find_top_element_with_left_member(levels, self)
            if elem_for_left:
                elem_for_left = self._descend_to_rightest(elem_for_left._left)
                elem_for_left._value += self._left._value
            elem_for_right = self._find_top_element_with_right_member(levels, self)
            if elem_for_right:
                elem_for_right = self._descend_to_leftest(elem_for_right._right)
                elem_for_right._value += self._right._value
            if levels[-1]._left is self:
                levels[-1]._left = IntValue(0)
            elif levels[-1]._right is self:
                levels[-1]._right = IntValue(0)
            else:
                raise ValueError
            return True
        for e in [self._left, self._right]:
            exploded = e._try_explode(levels + [e])
            if exploded:
                return exploded
        return False

    @classmethod
    def _find_top_element_with_left_member(cls, levels: list, excluded_elem):
        levels: typing.List[PairValue] = levels.copy()
        while levels:
            if levels[-1]._left is not excluded_elem:
                return levels[-1]
            excluded_elem = levels.pop()
        return None

    @classmethod
    def _find_top_element_with_right_member(cls, levels: list, excluded_elem):
        levels: typing.List[PairValue] = levels.copy()
        while levels:
            if levels[-1]._right is not excluded_elem:
                return levels[-1]
            excluded_elem = levels.pop()
        return None

    @classmethod
    def _descend_to_rightest(cls, elem):
        while isinstance(elem, PairValue):
            elem = elem._right
        elem: IntValue = elem
        return elem

    @classmethod
    def _descend_to_leftest(cls, elem):
        while isinstance(elem, PairValue):
            elem = elem._left
        elem: IntValue = elem
        return elem

    def _should_split(self) -> bool:
        return NotImplementedError

    def _try_split(self):
        # YES, I KNOW, THIS IS UGLY... BUT WORKING ! :-(
        if isinstance(self._left, IntValue):
            if self._left._should_split():
                self._left = self._left._try_split()
                return True
        elif self._left._try_split():
            return True
        if isinstance(self._right, IntValue):
            if self._right._should_split():
                self._right = self._right._try_split()
                return True
        elif self._right._try_split():
            return True
        return False

    @property
    def magnitude(self) -> int:
        return 3 * self._left.magnitude + 2 * self._right.magnitude

    def copy(self):
        return PairValue(self._left.copy(), self._right.copy())


def launch_tests():
    print("launch_tests() starting........")

    print("launch_tests() starting decode...")
    for val in """\
[1,2]
[[1,2],3]
[9,[8,7]]
[[1,9],[8,5]]
[[[[1,2],[3,4]],[[5,6],[7,8]]],9]
[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]
[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]""".split("\n"):
        val = val.strip()
        print(f"    Testing decode : {val}")
        val_decoded = SnailFishValue.decode(val)
        assert val_decoded
        assert isinstance(val_decoded, PairValue)
        assert str(val_decoded) == val
    print("launch_tests() finished decode")

    print("launch_tests() starting reducing...")
    for val_str, expected_output in [
        ("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]"),
        ("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]"),
        ("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]")
    ]:
        print(f"    Testing reduce {val_str} ; expected output = {expected_output}")
        val = SnailFishValue.decode(val_str)
        val.reduce()
        assert val == SnailFishValue.decode(expected_output), f"val = {val} / expected_output = {expected_output}"
    print("launch_tests() finished reducing")

    print("launch_tests() starting addition...")
    for list_inputs, expected_output in [
        (["[[[[4,3],4],4],[7,[[8,4],9]]]", "[1,1]"], "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"),
        (["[1,1]", "[2,2]", "[3,3]", "[4,4]"], "[[[[1,1],[2,2]],[3,3]],[4,4]]"),
        (["[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]"], "[[[[3,0],[5,3]],[4,4]],[5,5]]"),
        (["[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]", "[6,6]"], "[[[[5,0],[7,4]],[5,5]],[6,6]]"),
    ]:
        val = None
        expected_output = SnailFishValue.decode(expected_output)
        for it_val in list_inputs:
            it_val = SnailFishValue.decode(it_val)
            if val is None:
                print(f"    Testing sum : first item = {it_val}...")
                val = it_val
            else:
                print(f"    Testing sum : {val} + {it_val}...")
                val += it_val
            print(f"    Testing sum : result = {val}...")
        print(f"    Testing sum : output = {val} / expected_output = {expected_output}")
        assert val == expected_output, f"val = {val} / expected_output = {expected_output}"
    print("launch_tests() finished addition")

    print("launch_tests() finished !!!!!!!!!!!!!")


def main(is_part1: bool, input_file: str, verbose_level=0):
    list_snail_fish = []
    with open(input_file) as f:
        for line in f:
            # yeah... eval() works but it's a soooo bad idea in term of... everything!
            # list_snail_fish.append(eval(line.strip()))
            list_snail_fish.append(SnailFishValue.decode(line.strip()))
    if is_part1:
        snail_fish: typing.Optional[SnailFishValue] = None
        for it_snail_fish in list_snail_fish:
            if snail_fish is None:
                snail_fish = it_snail_fish
                continue
            snail_fish += it_snail_fish
        print(f"Final snail fish : {snail_fish}")
        print(f"Final magnitude = {snail_fish.magnitude}")
    else:
        max_magnitude = None
        for i in range(len(list_snail_fish)):
            for j in range(len(list_snail_fish)):
                #if j == i:
                #    continue
                fish_i = list_snail_fish[i].copy()
                fish_j = list_snail_fish[j].copy()
                if verbose_level >= 2:
                    print(f"i = {i:3d} / j = {j:3d} => fish_i = {fish_i} / fish_j = {fish_j}")
                res: SnailFishValue = fish_i + fish_j
                if verbose_level >= 2:
                    print(f"i = {i:3d} / j = {j:3d} => fish_i + fish_j = {res}")
                tmp_magnitude = res.magnitude

                print(f"i = {i:3d} / j = {j:3d} => magnitude = {tmp_magnitude}")
                if max_magnitude is None or max_magnitude < tmp_magnitude:
                    max_magnitude = tmp_magnitude
        print(f"Max possible magnitude = {max_magnitude}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("--tests", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    if args.tests:
        launch_tests()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
