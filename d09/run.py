import argparse
import typing


class Basin:
    def __init__(self, points: typing.Set[typing.Tuple[int, int]]):
        self._points = points

    def contains_point(self, coord: typing.Tuple[int, int]):
        return coord in self._points

    def add_point(self, coord: typing.Tuple[int, int]):
        self._points.add(coord)

    def merge_basin(self, other_basin):
        other_basin: Basin = other_basin
        self._points.update(other_basin._points)
        other_basin._points.clear()

    def __len__(self):
        return len(self._points)

    def __str__(self):
        return str(self._points)

    def __repr__(self):
        return f"Basin<len={len(self)}, points={self}>"


class CaveMap:
    def __init__(self, lines: typing.List[str], verbose_level=0):
        self._cave_map = [[int(x) for x in l] for l in lines]
        self.verbose_level = verbose_level

    def compute_part1(self) -> int:
        res = 0
        num_lines = len(self._cave_map)
        num_columns = len(self._cave_map[0])
        previous_line = None
        for idx_line in range(num_lines):
            line = self._cave_map[idx_line]
            for idx_col in range(num_columns):
                current_val = line[idx_col]
                if idx_col > 0 and current_val >= line[idx_col - 1]:
                    continue
                if idx_col < num_columns - 1 and current_val >= line[idx_col + 1]:
                    continue
                if previous_line and current_val >= previous_line[idx_col]:
                    continue
                if idx_line < num_lines - 1 and current_val >= self._cave_map[idx_line + 1][idx_col]:
                    continue
                res += (current_val + 1)
                if self.verbose_level >= 2:
                    print(f"[DEBUG]     Found minimum at coord = {(idx_col, idx_line)} : height = {current_val} => risk level = {current_val + 1} => total = {res}")
            previous_line = line  # make access quicker
        return res

    def compute_part2(self) -> int:
        num_lines = len(self._cave_map)
        num_columns = len(self._cave_map[0])
        list_basins: typing.List[Basin] = []
        previous_line = None
        for idx_line in range(num_lines):
            line = self._cave_map[idx_line]
            for idx_col in range(num_columns):
                coord = (idx_col, idx_line)
                if line[idx_col] == 9:
                    continue  # not part of a basin
                list_previous_points = []
                if idx_col > 0 and line[idx_col - 1] < 9:
                    list_previous_points.append((idx_col - 1, idx_line))
                if idx_line > 0 and previous_line[idx_col] < 9:
                    list_previous_points.append((idx_col, idx_line - 1))
                list_coord_matching_basins = set()
                for it_basin in list_basins:
                    for previous_coord in list_previous_points:
                        if self.verbose_level >= 3:
                            print(f"[DEBUG]     coord = {coord} / looking if previous point = {previous_coord} in basin {it_basin!r}")
                        if it_basin.contains_point(previous_coord):
                            if self.verbose_level >= 2:
                                print(f"[DEBUG]     coord = {coord} / FOUND previous point = {previous_coord} in basin {it_basin!r}")
                            list_coord_matching_basins.add(it_basin)
                            break
                if not list_coord_matching_basins:
                    if self.verbose_level >= 2:
                        print(f"[DEBUG]     starting a new basin at coord = {coord}")
                    list_basins.append(Basin({coord}))
                else:
                    # check if multiple basins, and if there are, merge them !
                    list_coord_matching_basins = list(list_coord_matching_basins)
                    if self.verbose_level >= 2:
                        print(f"[DEBUG]     {coord} / adding point to basin {list_coord_matching_basins[0]!r} .. other matching basins count = {len(list_coord_matching_basins) - 1}")
                    list_coord_matching_basins[0].add_point(coord)
                    for other_basin in list_coord_matching_basins[1:]:
                        if self.verbose_level >= 2:
                            print(f"[DEBUG]     {coord} / merging basin {other_basin!r} into basin {list_coord_matching_basins[0]!r}")
                        list_coord_matching_basins[0].merge_basin(other_basin)
                        list_basins.remove(other_basin)
            previous_line = line

        # TODO : we still need to merge adjacent points
        if self.verbose_level >= 1:
            print(f"[DEBUG] found {len(list_basins)} basins")
        basin_sizes = []
        for basin in list_basins:
            tmp_len = len(basin)
            if self.verbose_level >= 1:
                print(f"[DEBUG] basin len = {tmp_len}")
            basin_sizes.append(tmp_len)
        basin_sizes.sort()
        print(f"[DEBUG] list basin len = {basin_sizes}")
        res = 1
        for it_len in basin_sizes[-3:]:
            res *= it_len
        return res


def main(is_part1: bool, input_file: str, verbose_level=0):

    with open(input_file) as f:
        all_lines = []
        for line in f:
            all_lines.append(line.strip())
        cave_map = CaveMap(all_lines, verbose_level)

    if is_part1:
        res = cave_map.compute_part1()
        print(f"Result (sum of the risk levels): {res}")
    else:
        res = cave_map.compute_part2()
        print(f"Result (multiply together the sizes of the three largest basins): {res}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
