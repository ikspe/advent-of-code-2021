import argparse
import typing


class CaveMap:
    def __init__(self, verbose_level=0):
        self._caves: typing.Dict[str, typing.Set[str]] = {}
        self.verbose_level = verbose_level

    def add_link(self, cave1: str, cave2: str):
        for x, y in [(cave1, cave2), (cave2, cave1)]:
            cave_links = self._caves.get(x)
            if cave_links:
                cave_links.add(y)
            else:
                self._caves[x] = {y}

    def print(self):
        for cave, links in self._caves.items():
            print(f"cave {cave:5s} linked to : {links}")

    def find_all_paths(self, max_visit_small_caves: int) -> typing.List[typing.List[str]]:
        list_paths = []
        start = "start"
        end = "end"
        assert start in self._caves and end in self._caves
        current_path = [start]
        current_path_branches = [self._caves[current_path[-1]].copy()]
        while True:
            if not current_path:
                break
            if not current_path_branches[-1]:
                current_path.pop()
                current_path_branches.pop()
                continue
            next_dst = current_path_branches[-1].pop()
            if next_dst != next_dst.upper():
                if next_dst in current_path:
                    if next_dst == start:
                        if self.verbose_level >= 3:
                            print(f"Current path = {current_path} ; next_dst = {next_dst} => forbidden to visit {start} more than once")
                        continue
                    already_visited_count = len([x for x in current_path if x == next_dst])
                    if already_visited_count >= max_visit_small_caves:
                        if self.verbose_level >= 3:
                            print(f"Current path = {current_path} ; next_dst = {next_dst} => small cave already visited {already_visited_count} times, impossible to visit once more : max = {max_visit_small_caves} times")
                        continue
                already_visited_small_caves = {}
                for c in current_path + [next_dst]:
                    if c.upper() == c:
                        continue
                    already_visited_small_caves[c] = already_visited_small_caves.get(c, 0) + 1
                number_of_small_caves_too_visited = len([k for k, v in already_visited_small_caves.items() if v >= max_visit_small_caves])
                if number_of_small_caves_too_visited > 1:
                    if self.verbose_level >= 3:
                        print(f"Current path = {current_path} ; next_dst = {next_dst} => small caves already visited more than {max_visit_small_caves} times = {number_of_small_caves_too_visited} = {already_visited_small_caves}")
                    continue
            if self.verbose_level >= 3:
                print(f"Current path = {current_path} ; next_dst = {next_dst}")
            current_path.append(next_dst)
            current_path_branches.append(self._caves[next_dst].copy())
            if next_dst == end:
                list_paths.append(current_path.copy())
                if self.verbose_level >= 2:
                    print(f"Current path = {current_path} ; reached destination !")
                current_path.pop()
                current_path_branches.pop()
                continue
        return list_paths


def main(is_part1: bool, input_file: str, verbose_level=0):
    cave_map = CaveMap(verbose_level=verbose_level)
    with open(input_file) as f:
        for line in f:
            cave_map.add_link(*line.strip().split("-"))
            continue
    if verbose_level >= 1:
        cave_map.print()
    if is_part1:
        max_visit_small_caves = 1
    else:
        max_visit_small_caves = 2
    all_paths = cave_map.find_all_paths(max_visit_small_caves)
    if verbose_level >= 1:
        for path in all_paths:
            print(",".join(path))
    print(f"Result : paths count = {len(all_paths)}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
