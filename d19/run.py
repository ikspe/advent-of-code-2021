import argparse
import typing


# list_rotation_functions: typing.List[typing.Tuple[callable, str]] = [((lambda pos: f(*pos)), name + "_") for f, name in [
list_rotation_functions: typing.Dict[str, callable] = {
    "01": lambda x,y,z: (x,y,z),
    "02": lambda x,y,z: (x,z,-y),
    "03": lambda x,y,z: (x,-y,-z),
    "04": lambda x,y,z: (x,-z,y),

    "05": lambda x,y,z: (y,-x,z),
    "06": lambda x,y,z: (y,z,x),
    "07": lambda x,y,z: (y,x,-z),
    "08": lambda x,y,z: (y,-z,-x),

    "09": lambda x,y,z: (-x,-y,z),
    "10": lambda x,y,z: (-x,z,y),
    "11": lambda x,y,z: (-x,y,-z),
    "12": lambda x,y,z: (-x,-z,-y),

    "13": lambda x,y,z: (-y,x,z),
    "14": lambda x,y,z: (-y,z,-x),
    "15": lambda x,y,z: (-y,-x,-z),
    "16": lambda x,y,z: (-y,-z,x),

    "17": lambda x,y,z: (-z,y,x),
    "18": lambda x,y,z: (-z,x,-y),
    "19": lambda x,y,z: (-z,-y,-x),
    "20": lambda x,y,z: (-z,-x,y),

    "21": lambda x,y,z: (z,y,-x),
    "22": lambda x,y,z: (z,-x,-y),
    "23": lambda x,y,z: (z,-y,x),
    "24": lambda x,y,z: (z,x,y),
}
# ]]
# list_rotation_functions: typing.List[typing.Tuple[callable, str]] = [(lambda pos: f(*pos), name) for f, name in list_rotation_functions]


def repr_one_probe(pos):
    return ",".join([str(x) for x in pos])


def print_probes(list_probes):
    for pos in list_probes:
        print(repr_one_probe(pos))


def test_rotations():
    print("Testing rotations...")
    assert len(list_rotation_functions) == 24
    pos = (11, 22, 33)
    first_f_name = "01"
    first_f = list_rotation_functions[first_f_name]
    print(f"pos = {pos}  ==>  {first_f_name} result = {first_f(*pos)}")
    print(f"Asserting all results are different...")
    for pos in [(1, 2, 3), (1, 3, 9)]:
        list_result = [f(*pos) for f_name, f in list_rotation_functions.items()]
        assert all([sum([abs(x) for x in res]) == sum([abs(x) for x in pos]) for res in list_result])
        print(f"pos = {pos}  ==>  list_result = {list_result}")
        assert len(list_result) == 24
        assert len(set(list_result)) == 24
    print(f"Asserting all rotations applied 3 or 4 times = identity...")
    for pos in [(1, 2, 3), (1, 3, 9)]:
        for f_name, f in list_rotation_functions.items():
            res_3 = pos
            for i in range(3):
                res_3 = f(*res_3)
            res_4 = f(*res_3)
            print(f"Checking with function {f_name} and pos = {pos}...")
            print(f"pos = {pos}  ==>  res_3 = {res_3} / res_4 = {res_4}")
            assert res_3 == pos or res_4 == pos, f"pos = {pos} / f_name = {f_name} / res_3 = {res_3} / res_4 = {res_4}"
    print(f"... Finished Asserting all rotations applied 3 or 4 times = identity")
    print("Checking rotations functions are correct...")
    input_test = (1, 2, 4)
    rota_x = lambda x, y, z: (x, z, -y)
    rota_y = lambda x, y, z: (-z, y, x)
    rota_z = lambda x, y, z: (y, -x, z)
    all_rotations = set()
    candidates = {input_test}
    while candidates:
        curr = candidates.pop()
        all_rotations.add(curr)
        for curr in [rota_x(*curr), rota_y(*curr), rota_z(*curr)]:
            print(f"Got rotation = {curr}")
            if curr not in all_rotations:
                print(f"Rotation added to all_rotations")
                candidates.add(curr)
                all_rotations.add(curr)
    del candidates
    print(f"len(all_rotations) = {len(all_rotations)} : {all_rotations}")
    assert len(all_rotations) == 24
    all_rotations_from_functions = set()
    for rota_name, f_rota in list_rotation_functions.items():
        output_rota = f_rota(*input_test)
        print(f"Checking rota function {rota_name} with input = {input_test} / output = {output_rota}")
        all_rotations_from_functions.add(output_rota)
        assert output_rota in all_rotations
    assert len(all_rotations_from_functions) == 24
    assert all_rotations_from_functions == all_rotations

    print("... Finished Checking rotations functions are correct")
    print("... Finished testing rotations")


class Scanner:
    def __init__(self, name: str):
        self.name = name
        self._position = None
        self._probes: typing.List[typing.Tuple[int, int, int]] = []
        self._probes_rotation = {rotation_name: [] for rotation_name in list_rotation_functions}
        self._probes_absolute_coord: typing.List[typing.Tuple[int, int, int]] = []

    @property
    def position(self) -> typing.Optional[typing.Tuple[int, int, int]]:
        return self._position

    @property
    def probes(self) -> typing.List[typing.Tuple[int, int, int]]:
        return self._probes

    def get_probes_rotation(self, rotation_name: str) -> typing.List[typing.Tuple[int, int, int]]:
        return self._probes_rotation[rotation_name]

    @property
    def probes_absolute_coord(self) -> typing.List[typing.Tuple[int, int, int]]:
        return self._probes_absolute_coord

    def add_probe(self, pos):
        self.probes.append(pos)
        for rotation_name, f in list_rotation_functions.items():
            self._probes_rotation[rotation_name].append(f(*pos))

    def set_absolute_position(self, absolute_pos: typing.Tuple[int, int, int], list_probes_absolute_coord: list):
        self._position = absolute_pos
        self._probes_absolute_coord = list_probes_absolute_coord


def try_match_scanners(list_scanners: typing.List[Scanner]):
    list_scanners_known = [x for x in list_scanners if x.position is not None]
    list_scanners_unknown = [x for x in list_scanners if x.position is None]
    print(f"            try_match_scanners() / len(list_scanners) = {len(list_scanners)} / len(list_scanners_known) = {len(list_scanners_known)} / len(list_scanners_unknown) = {len(list_scanners_unknown)}")
    for scanner_unknown_pos in list_scanners_unknown:
        for scanner_known_pos in list_scanners_known:
            matching_result = are_matching_scanners(scanner_known_pos, scanner_unknown_pos)
            if not matching_result:
                continue
            print(f"Found matching scanners : {scanner_known_pos.name} -> {scanner_unknown_pos.name} / found position = {matching_result[0]}")
            assert len(matching_result[0]) == 3
            assert len(matching_result[1]) == len(scanner_unknown_pos.probes)
            scanner_unknown_pos.set_absolute_position(matching_result[0], matching_result[1])
            print("Probes absolute positions :")
            print_probes(matching_result[1])
            return True

    return False


def are_matching_scanners(scanner_known_pos: Scanner, scanner_unknown_pos: Scanner):
    assert scanner_known_pos.position is not None
    assert scanner_known_pos.probes_absolute_coord
    assert scanner_unknown_pos.position is None
    assert not scanner_unknown_pos.probes_absolute_coord

    for rotation_name in list_rotation_functions:
        print(f"Trying rotation {rotation_name} for scanners = {scanner_known_pos.name} / {scanner_unknown_pos.name}")
        list_unknown_probes_absolute_coord = scanner_unknown_pos.get_probes_rotation(rotation_name)
        # for it_pos in list_unknown_probes_absolute_coord[0:2]:
        #     print(f"Position probe after rotation '{f_rotation_name}' : {repr_one_probe(it_pos)}")
        # print_probes(list_unknown_probes_absolute_coord)
        # print()
        res_matching_probes = are_matching_probes(scanner_known_pos.probes_absolute_coord, list_unknown_probes_absolute_coord)
        if res_matching_probes:
            print(f"Found matching scanners : {scanner_known_pos.name} -> {scanner_unknown_pos.name} for rotation {rotation_name} : origin = {repr_one_probe(res_matching_probes[0])}")
            return res_matching_probes

    return None


def are_matching_probes(list_known_probes: list, list_unknown_probes: list) -> typing.Optional[typing.Tuple[typing.Tuple[int, int, int], typing.List[typing.Tuple[int, int, int]]]]:
    # print(f"are_matching_probes() : len(list_known_probes) = {len(list_known_probes)} / len(list_unknown_probes) = {len(list_unknown_probes)}")
    for i in range(len(list_known_probes)):
        ref_i = list_known_probes[i]
        for j in range(len(list_unknown_probes)):
            ref_j = list_unknown_probes[j]
            shift_probes_to_match = tuple([ref_i[x] - ref_j[x] for x in range(3)])
            count, possible_probes_position = count_matching_probes(list_known_probes, list_unknown_probes, shift_probes_to_match)
            if count >= 12:
                print(f"Found matching probes i = {i:2d} / j = {j:2d}... ref_i = {repr_one_probe(ref_i)} / ref_j = {repr_one_probe(ref_j)} / shift_probes_to_match = {repr_one_probe(shift_probes_to_match)}  ==> count match = {count}")
                return shift_probes_to_match, possible_probes_position
    return None


def count_matching_probes(list_probes_ref, list_probes_to_match, shift_probes_to_match):
    list_probes_to_match = [tuple([pos[x] + shift_probes_to_match[x] for x in range(3)]) for pos in list_probes_to_match]
    count = 0
    for i in range(len(list_probes_ref)):
        ref_i = list_probes_ref[i]
        for j in range(len(list_probes_to_match)):
            if ref_i == list_probes_to_match[j]:
                count += 1
                if count >= 12:
                    return count, list_probes_to_match
                break
    return count, list_probes_to_match


def main(is_part1: bool, input_file: str, verbose_level=0, cheating_order_first=None, cheating_order_last=None, cheating_unmatched=None):
    list_scanners: typing.List[Scanner] = []
    current_scanner = None
    with open(input_file) as f:
        for line in f:
            line = line.strip()
            if "scanner" in line:
                tokens = line.split("---")
                assert len(tokens) == 3
                scanner_name = tokens[1].strip()
                del tokens
                current_scanner = Scanner(scanner_name)
                list_scanners.append(current_scanner)
                continue
            if not line:
                continue
            pos = tuple([int(x) for x in line.split(",")])
            assert len(pos) == 3
            current_scanner.add_probe(pos)
    del current_scanner

    if verbose_level >= 1:
        print("Printing list of scanners...")
        for it_scanner in list_scanners:
            print(f"--- {it_scanner.name} ---")

    if cheating_order_first:
        cheating_list = []
        for cheating_name in cheating_order_first.split(","):
            print(f"Cheating... checking first = {cheating_name}")
            found = False
            for it_scanner in list_scanners:
                if it_scanner.name in (cheating_name, "scanner " + cheating_name):
                    list_scanners.remove(it_scanner)
                    cheating_list.append(it_scanner)
                    found = True
                    break
            if not found:
                raise ValueError(f"Unknown cheating scanner {cheating_name}")
        list_scanners = cheating_list + list_scanners
        del cheating_list

    if cheating_order_last:
        cheating_list = []
        for cheating_name in cheating_order_last.split(","):
            print(f"Cheating... checking last = {cheating_name}")
            found = False
            for it_scanner in list_scanners:
                if it_scanner.name in (cheating_name, "scanner " + cheating_name):
                    list_scanners.remove(it_scanner)
                    cheating_list.append(it_scanner)
                    found = True
                    break
            if not found:
                raise ValueError(f"Unknown cheating scanner {cheating_name}")
        list_scanners = list_scanners + cheating_list
        del cheating_list

    list_unmatched_scanners = []
    if cheating_unmatched:
        for cheating_name in cheating_unmatched.split(","):
            print(f"Cheating... checking unmatched = {cheating_name}")
            found = False
            for it_scanner in list_scanners:
                if it_scanner.name in (cheating_name, "scanner " + cheating_name):
                    list_scanners.remove(it_scanner)
                    list_unmatched_scanners.append(it_scanner)
                    found = True
                    break
            if not found:
                raise ValueError(f"Unknown cheating scanner {cheating_name}")

    if verbose_level >= 1:
        print("Printing list of scanners...")
        for it_scanner in list_scanners:
            print(f"--- {it_scanner.name} ---")

    if verbose_level >= 2:
        print("Printing list of probes...")
        for it_scanner in list_scanners:
            print(f"--- {it_scanner.name} ---")
            for pos in it_scanner.probes:
                print(",".join([str(x) for x in pos]))
            print("")
        print("... Finished Printing list of probes")

    # first scanner has arbitrary position (0, 0, 0)
    list_scanners[0].set_absolute_position((0, 0, 0), [pos for pos in list_scanners[0].probes])
    while not all([x.position is not None for x in list_scanners]):
        if not try_match_scanners(list_scanners):
            raise ValueError("No probes have matched ! But there were expecting to do so...")
    print("=" * 64)

    scanners_list_pos = [x.position for x in list_scanners]
    print(f"Found scanners absolute coords = {scanners_list_pos}")
    set_all_probes = set()
    for scanner in list_scanners:
        set_all_probes.update(scanner.probes_absolute_coord)

    # part 1
    print("=" * 64)
    print(f"Found {len(set_all_probes)} probes")
    print(f"Found probes = {set_all_probes}")
    if list_unmatched_scanners:
        print(f"Found unmatched scanners = {[x.name for x in list_unmatched_scanners]}")
        print(f"Found {sum([len(x.probes) for x in list_unmatched_scanners])} unmatched probes")
        print("=" * 64)

    # part 2
    print("=" * 64)
    max_distance = None
    while scanners_list_pos:
        scanner = scanners_list_pos.pop()
        for scanner2 in scanners_list_pos:
            dist = sum([abs(scanner2[x] - scanner[x]) for x in range(3)])
            if max_distance is None or dist > max_distance:
                max_distance = dist

    print(f"Max manatthan distance between 2 scanners = {max_distance}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")
    parser.add_argument("--tests", default=False, action="store_true")
    parser.add_argument("--cheating-order-first")  # to fasten the lookup ; with mu input, first are : 0,2,18,7,23,10,15,1,24,26,27,4,12
    parser.add_argument("--cheating-order-last")  # to fasten the lookup ; with my input, last are : 8,30,31,32,3,6,9,13,5,17,19,20,21,34,35,36,16,29,33,25,14,28,22,11
    parser.add_argument("--cheating-unmatched")  # to fasten the lookup ; scanner 11 doesn't match with any other
    parser.add_argument("--no-run", dest="run", default=True, action="store_false")

    # parse the arguments
    args = parser.parse_args()

    if args.tests:
        test_rotations()

    # run !
    if args.run:
        import time
        time_begin = time.time()
        try:
            main(args.part1, args.input, verbose_level=args.verbose,
                 cheating_order_first=args.cheating_order_first, cheating_order_last=args.cheating_order_last, cheating_unmatched=args.cheating_unmatched)
        finally:
            time_end = time.time()
            print(f"Total execution time = {time_end - time_begin} s")
