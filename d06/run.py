import argparse
import typing


class Population:

    _COUNTER_AFTER_SEX = 6
    _COUNTER_NEW_BORN = 8

    def __init__(self, line_init: str):
        self._dict_sex: typing.Dict[int, int] = {}
        for sex_time in [int(x) for x in line_init.split(",")]:
            self._dict_sex[sex_time] = self._dict_sex.get(sex_time, 0) + 1

    def print_state(self, day: int):
        if day:
            str_prefix = f"After {day:2d} days"
        else:
            str_prefix = "Initial state"
        str_data = ",".join([",".join([str(fuck_time)] * nb_fish) for fuck_time, nb_fish in self._dict_sex.items()])
        print(f"{str_prefix}: {str_data}")

    @classmethod
    def _append_fishes(cls, dict_sex, sex_date, nb_fish):
        dict_sex[sex_date] = dict_sex.get(sex_date, 0) + nb_fish

    def launch_meetic(self):
        new_sex_time = {}
        for sex_time, nb_fish in self._dict_sex.items():
            if sex_time == 0:
                self._append_fishes(new_sex_time, self._COUNTER_AFTER_SEX, nb_fish)
                self._append_fishes(new_sex_time, self._COUNTER_NEW_BORN, nb_fish)
            else:
                self._append_fishes(new_sex_time, sex_time - 1, nb_fish)
        self._dict_sex = new_sex_time

    def __len__(self):
        return sum(self._dict_sex.values())


def main(is_part1: bool, input_file: str, verbose_level=0):

    population = None
    if is_part1:
        max_days = 80
    else:
        max_days = 256

    with open(input_file) as f:
        for line in f:
            population = Population(line.strip())
            break  # only 1 line in file

    assert population
    day = 0
    if verbose_level >= 1:
        population.print_state(day)
    for day in range(1, max_days+1):
        population.launch_meetic()
        if verbose_level >= 1:
            population.print_state(day)

    print(f"Final state at day = {day} : total of {len(population)} fish")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
