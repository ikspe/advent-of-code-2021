# Part 1

## Example

```
$ time python3 run.py --part1 --input input-example.txt 
Final state at day = 80 : total of 5934 fish

real	0m0,065s
user	0m0,058s
sys	0m0,008s
```

## Real data

```
$ time python3 run.py --part1 --input ../../advent-of-code-2021-inputs/d06/input.txt 
Final state at day = 80 : total of 345387 fish

real	0m0,054s
user	0m0,045s
sys	0m0,008s
```

# Part 2

## Example

```
$ time python3 run.py --input input-example.txt 
Final state at day = 256 : total of 26984457539 fish

real	0m0,069s
user	0m0,057s
sys	0m0,012s
```

## Real data

```
$ time python3 run.py --input ../../advent-of-code-2021-inputs/d06/input.txt 
Final state at day = 256 : total of 1574445493136 fish

real	0m0,065s
user	0m0,053s
sys	0m0,012s
```
