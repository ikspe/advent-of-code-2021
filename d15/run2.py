import argparse
import typing


class Grid:
    def __init__(self, lines, verbose_level=0):
        self._grid = [[int(x) for x in line] for line in lines]
        self.verbose_level = verbose_level
        self._nb_lines = len(self._grid)
        self._nb_cols = len(self._grid[0])

    def enlarge_your_map(self, scaling_factor: int):
        # enlarge horizontally
        for y in range(len(self._grid)):
            line = self._grid[y]
            new_line = []
            for i in range(scaling_factor):
                new_line += [(x + i - 1) % 9 + 1 for x in line]
            self._grid[y] = new_line
        # enlarge vertically
        new_grid = []
        for i in range(scaling_factor):
            for line in self._grid:
                new_grid.append([(x + i - 1) % 9 + 1 for x in line])
        self._grid = new_grid
        self._nb_lines = len(self._grid)
        self._nb_cols = len(self._grid[0])

    def find_best_path(self):
        # start_pos = (0, 0)
        end_pos = (self._nb_cols - 1, self._nb_lines - 1)
        if self.verbose_level >= 2:
            print(f"[DEBUG] end_pos = {end_pos}")

        path_lowest_risk: typing.Optional[typing.List[typing.Tuple[int, int]]] = None

        current_path: typing.List[typing.Tuple[int, int]] = [(0, 0)]
        set_current_path = set(current_path)  # faster lookup
        current_options: typing.List[typing.List[typing.Tuple[int, int]]] =\
            [[(1, 0), (0, 1)]]

        dict_pos_lowest_risks: typing.Dict[typing.Tuple[int, int], int] = {}

        lowest_risk_at_end = None

        current_risk = 0
        # count_tried_path = 0
        while True:
            # if self.verbose_level >= 2 and count_tried_path % 100 == 0:
            #     print(f"[DEBUG] count_tried_path = {count_tried_path:4d} / current_risk = {current_risk:4d} / lowest_risk = {dict_pos_lowest_risks.get(end_pos)} / current_path = {current_path}")
            if not current_options:
                if self.verbose_level >= 2:
                    print(f"[DEBUG] STOPPING !")
                break
            if self.verbose_level >= 3:
                print(f"[DEBUG] current_pos = {current_path[-1]} / current_risk = {current_risk} / current_path = {current_path} / current_options = {current_options[-1]}")
            if not current_options[-1]:
                if self.verbose_level >= 3:
                    print(f"[DEBUG]     current_pos = {current_path[-1]} => no more options")
                old_x, old_y = current_path.pop()
                set_current_path.remove((old_x, old_y))
                current_options.pop()
                current_risk -= self._grid[old_y][old_x]
                continue
            new_pos = current_options[-1].pop(0)
            new_x, new_y = new_pos
            new_risk = current_risk + self._grid[new_pos[1]][new_pos[0]]
            # count_tried_path += 1
            existing_risk = dict_pos_lowest_risks.get(new_pos)
            if existing_risk is not None and new_risk >= existing_risk:
                if self.verbose_level >= 3:
                    print(f"[DEBUG]     new_pos = {new_pos} ; new_risk ({new_risk}) >= lowest_risk ({existing_risk}) : skipped")
                continue  # don't go there
            if lowest_risk_at_end is not None and new_risk >= lowest_risk_at_end:
                if self.verbose_level >= 3:
                    print(f"[DEBUG]     new_pos = {new_pos} ; new_risk ({new_risk}) >= lowest_risk_at_end ({lowest_risk_at_end}) : skipped")
                continue  # don't go there
            current_path.append(new_pos)
            set_current_path.add(new_pos)
            current_options.append([(x, y)
                                    for x, y in [
                                        (new_x + 1, new_y), (new_x, new_y + 1),
                                        # (new_x - 1, new_y), (new_x, new_y - 1),  # TODO (1) : this is a dangerous assumption that shortest path in always in this direction, doesn't work with my data set
                                    ] if (0 <= x < self._nb_cols) and (0 <= y < self._nb_lines)
                                    # and (x, y) not in set_current_path  # TODO (2) : not needed if TODO (1) is commented
                                    ])
            current_risk = new_risk
            dict_pos_lowest_risks[new_pos] = new_risk
            if self.verbose_level >= 3:
                print(f"[DEBUG]     new pos = {current_path[-1]} / next options = {current_options[-1]}")
            if new_pos == end_pos:
                lowest_risk_at_end = new_risk
                if self.verbose_level >= 2:
                    print(f"[DEBUG] REACHED end_pos = {end_pos} with risk = {current_risk} and path = {current_path}!")
                path_lowest_risk = current_path.copy()
        return path_lowest_risk, lowest_risk_at_end

    def draw(self):
        for line in self._grid:
            print("".join([str(x) for x in line]))


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        grid = Grid([line.strip() for line in f.readlines()], verbose_level)
    if is_part1:
        pass
    else:
        grid.enlarge_your_map(5)
        # grid.draw()
    path_lowest_risk, lowest_risk = grid.find_best_path()
    if verbose_level >= 1:
        print(f"Lowest total risk = {lowest_risk} for path = {path_lowest_risk}")
    return lowest_risk


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    import time
    time_begin = time.time()
    res = main(args.part1, args.input, verbose_level=args.verbose)
    time_end = time.time()
    print(f"Execution time = {time_end - time_begin}")
    print(f"Result = {res}")
