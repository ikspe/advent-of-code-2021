import argparse
import typing


class Universe:
    def __init__(self, players: typing.List[typing.List[int]], occurrences=1):
        self._players = [p.copy() for p in players]
        self._occurrences = occurrences

    @property
    def id(self):
        return self._players[0][1], self._players[0][2], self._players[1][1], self._players[1][2]

    def go_forward(self, player_idx: int, value: int):
        player = self._players[player_idx]
        player[1] = (player[1] + value - 1) % GameBoard.SPACE_COUNT + 1

    def compute_score(self, player_idx: int) -> bool:
        player = self._players[player_idx]
        player[2] += player[1]
        return player[2] >= GameBoard.POINTS_VICTORY

    @property
    def players(self):
        return self._players

    def increase_occurrences(self, nb: int):
        self._occurrences += nb

    @property
    def number_of_occurrences(self):
        return self._occurrences


class DeterministDice:
    def __init__(self, start_pos=1, max_val=100):
        self._current_pos = start_pos
        self._max_val = max_val
        self._count_roll = 0

    def roll(self):
        res = self._current_pos
        self._current_pos = self._current_pos % self._max_val + 1
        self._count_roll += 1
        return res

    @property
    def count_roll(self):
        return self._count_roll


class GameBoard:
    SPACE_COUNT = 10
    POINTS_VICTORY = None
    NB_PLAYERS = None


def print_player(player):
    assert len(player) == 3
    print(f"    Player {player[0]} ; position = {player[1]:2d} ; score = {player[2]:4d}")


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        players = []  # list of 3 values : [name, position, score]
        for line in f:
            line = line.strip()
            if not line:
                continue
            tokens = line.split("starting position:")
            assert len(tokens) == 2
            players.append([int(tokens[0].split("Player")[1].strip()), int(tokens[1]), 0])
        universe = Universe(players)
        GameBoard.NB_PLAYERS = len(players)
        del players
    assert len(universe.players) == 2 and GameBoard.NB_PLAYERS == 2

    if verbose_level >= 1:
        print("Players initial position:")
        for p in universe.players:
            print_player(p)

        print("Starting to play:")

    if is_part1:
        GameBoard.POINTS_VICTORY = 1000
        dice = DeterministDice(start_pos=1)
        player_idx = -1
        while True:
            player_idx = (player_idx + 1) % GameBoard.NB_PLAYERS
            player = universe.players[player_idx]
            if verbose_level >= 2:
                print("Player before playing:")
                print_player(player)
            for i in range(3):
                dice_value = dice.roll()
                universe.go_forward(player_idx, dice_value)
                if verbose_level >= 2:
                    print(f"                        Dice rolling with value {dice_value} ; pdice has rolled {dice.count_roll} times")
                if verbose_level >= 2:
                    print_player(player)
            has_won = universe.compute_score(player_idx)
            if verbose_level >= 2:
                print("Player after playing:")
                print_player(player)
            if has_won:
                if verbose_level >= 1:
                    print("Player has won:")
                    print_player(player)
                break
        min_players_score = min([p[2] for p in universe.players])
        count_dice_roll = dice.count_roll
        if verbose_level >= 1:
            print(f"Min players score : {min_players_score}")
            print(f"Number of times the die was rolled during the game : {count_dice_roll}")
            print(f"Result = {min_players_score} x {count_dice_roll} = {min_players_score * count_dice_roll}")
        return min_players_score * count_dice_roll

    else:
        GameBoard.POINTS_VICTORY = 21

        tmp_roll_3_dices = [i + j + k for i in range(1, 3+1) for j in range(1, 3+1) for k in range(1, 3+1)]
        dices_3_occurrences = {}
        for v in tmp_roll_3_dices:
            dices_3_occurrences[v] = dices_3_occurrences.get(v, 0) + 1
        del tmp_roll_3_dices
        assert len(dices_3_occurrences) == 7
        assert sum(dices_3_occurrences.values()) == 3 ** 3

        count_winners = {}

        # to be smart grouping universes with index : (pos1, score1, pos2, score2) + corresponding universes count
        list_universes: typing.Dict[typing.Tuple, Universe] = {universe.id: universe}
        del universe
        player_idx = -1

        while list_universes:
            player_idx = (player_idx + 1) % GameBoard.NB_PLAYERS
            
            list_new_universes: typing.Dict[typing.Tuple, Universe] = {}
            for universe in list_universes.values():
                for value_3_dices, dice_occurrences in dices_3_occurrences.items():
                    new_universe = Universe(universe.players, occurrences=universe.number_of_occurrences * dice_occurrences)
                    new_universe.go_forward(player_idx, value_3_dices)
                    has_won = new_universe.compute_score(player_idx)
                    if has_won:
                        if verbose_level >= 2:
                            print(f"        Player {new_universe.players[player_idx]} (idx {player_idx}) has won in a universe with {new_universe.number_of_occurrences} occurrences")
                        count_winners[player_idx] = count_winners.get(player_idx, 0) + new_universe.number_of_occurrences
                        if verbose_level >= 2:
                            print(f"        Winning universes per player : {count_winners}")
                        continue
                    new_id = new_universe.id
                    existing_universe = list_new_universes.get(new_id)
                    if existing_universe:
                        existing_universe.increase_occurrences(new_universe.number_of_occurrences)
                    else:
                        list_new_universes[new_id] = new_universe
                    if verbose_level >= 2:
                        print(f"Created a new universe for value_3_dices = {value_3_dices} ; len(list_universes) = {len(list_universes)}")

            """
            for _ in range(3):
                list_new_universes: typing.Dict[typing.Tuple, Universe] = {}
                for universe in list_universes.values():
                    for dirac_val in range(1, 3 + 1):
                        new_universe = Universe(universe.players, occurrences=universe.number_of_occurrences)
                        new_universe.go_forward(player_idx, dirac_val)
                        new_id = new_universe.id
                        existing_universe = list_new_universes.get(new_id)
                        if existing_universe:
                            existing_universe.increase_occurrences(new_universe.number_of_occurrences)
                        else:
                            list_new_universes[new_id] = new_universe
                        if verbose_level >= 2:
                            print(f"Created a new universe for dirac_val = {dirac_val} ; len(list_universes) = {len(list_universes)}")
                list_universes = list_new_universes
                if verbose_level >= 2:
                    print(f"New universe len : len(list_universes) =  {len(list_universes)}")

            list_new_universes: typing.Dict[typing.Tuple, Universe] = {}
            for universe in list_universes.values():
                has_won = universe.compute_score(player_idx)
                if has_won:
                    if verbose_level >= 1:
                        print(f"        Player {universe.players[player_idx]} (idx {player_idx}) has won in a universe with {universe.number_of_occurrences} occurences")
                    count_winners[player_idx] = count_winners.get(player_idx, 0) + universe.number_of_occurrences
                    if verbose_level >= 1:
                        print(f"        Winning universes per player : {count_winners}")
                    continue
                new_id = universe.id
                existing_universe = list_new_universes.get(new_id)
                if existing_universe:
                    existing_universe.increase_occurrences(universe.number_of_occurrences)
                else:
                    list_new_universes[new_id] = universe
            """

            list_universes = list_new_universes
            if verbose_level >= 1:
                print(f"New universe len : len(list_universes) =  {len(list_universes)}")

        max_val_winning_universes = max(count_winners.values())
        if verbose_level >= 1:
            if list_universes:
                print(f"Remaining universes : {len(list_universes)}")
                for universe in list_universes.values():
                    print(f"- Universe {universe.id} with count {universe.number_of_occurrences}")
                    for player in universe.players:
                        print(f"  - Player {player[0]} / position {player[1]:2d} / score {player[2]:2d}")
            print(f"Count winners = {count_winners}")
            print(f"Number of universes with the most victories = {max_val_winning_universes}")
        return max_val_winning_universes


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    import time
    res = None
    time_begin = time.time()
    try:
        res = main(args.part1, args.input, verbose_level=args.verbose)
    finally:
        time_end = time.time()
        print(f"Result = {res}")
        print(f"Computation time = {time_end - time_begin} s")
