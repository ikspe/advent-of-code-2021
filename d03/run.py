import argparse
import typing


def compute_number_from_bits(bits_list: tuple):
    result = 0
    for x in bits_list:
        result *= 2
        result += x
    return result


def read_from_file(input_file: str, verbose_level=0) -> typing.List[tuple]:
    list_values = []
    with open(input_file) as f:
        for line in f:
            if verbose_level >= 2:
                print(f"    [DEBUG] read_from_file() : line = {line.strip()} => {tuple(x for x in line.strip())}")
            list_values.append(tuple(int(x) for x in line.strip()))
    return list_values


def find_most_common_bits(list_values: typing.List[tuple], verbose_level=0) -> tuple:
    line_count = 0
    count_bits1 = None
    for cur_values in list_values:
        if count_bits1 is None:
            count_bits1 = [0] * len(cur_values)
        assert len(count_bits1) == len(cur_values)
        if verbose_level >= 2:
            print(f"    [DEBUG] cur_values = {cur_values} / count_bits1 = {count_bits1} / line_count = {line_count}")
        for idx in range(len(count_bits1)):
            if cur_values[idx] == 1:
                count_bits1[idx] += 1
        line_count += 1
        continue
    if verbose_level >= 2:
        print(f"    [DEBUG] final: count_bits1 = {count_bits1} / line_count = {line_count}")
    return tuple(1 if x >= line_count / 2 else 0 for x in count_bits1)


def find_least_common_bits_from_most_common(most_common_bits: tuple) -> tuple:
    return tuple(0 if x == 1 else 1 for x in most_common_bits)


def filter_values(list_values: typing.List[tuple], idx, val, verbose_level=0) -> typing.List[tuple]:
    if verbose_level >= 1:
        print(f"    [DEBUG] building filtered list with idx = {idx} / val = {val} / len(list) = {len(list_values)}...")
    list_filtered_values = []
    for cur_values in list_values:
        if cur_values[idx] == val:
            list_filtered_values.append(cur_values)
    print(f"    [DEBUG] finished building filtered list : len = {len(list_filtered_values)}...")
    return list_filtered_values


def main(is_part1: bool, input_file: str, verbose_level=0):
    if verbose_level >= 1:
        print(f"    [DEBUG] Reading from file...")
    list_values = read_from_file(input_file, verbose_level)

    if verbose_level >= 1:
        print(f"    [DEBUG] Computing most/least common bits...")
    most_common_bits = find_most_common_bits(list_values, verbose_level)
    least_common_bits = find_least_common_bits_from_most_common(most_common_bits)

    if verbose_level >= 1:
        print(f"    [DEBUG] Finding gamma rate and epsilon rate...")
    gamma_rate = compute_number_from_bits(most_common_bits)
    epsilon_rate = compute_number_from_bits(least_common_bits)

    print(f"final part 1: most_common_bits = {most_common_bits} / least_common_bits = {least_common_bits}")
    print(f"final part 1: gamma_rate = {gamma_rate} / epsilon_rate = {epsilon_rate}")
    print(f"final part 1: result = {gamma_rate} * {epsilon_rate} = {gamma_rate * epsilon_rate}")

    if is_part1:
        return

    if verbose_level >= 1:
        print(f"    [DEBUG] Finding oxygen generator rating...")
    most_common_bits_filtered = most_common_bits
    list_values_filtered = list_values
    for idx in range(len(most_common_bits)):
        list_values_filtered = filter_values(list_values_filtered, idx, most_common_bits_filtered[idx], verbose_level)
        assert list_values_filtered
        if len(list_values_filtered) == 1:
            if verbose_level >= 1:
                print(f"    [DEBUG] Got list with single element : {list_values_filtered}...")
            break
        most_common_bits_filtered = find_most_common_bits(list_values_filtered, verbose_level)

    assert len(list_values_filtered) == 1

    print(f"final part 2: oxygen generator rating (raw val) = {list_values_filtered[0]}")
    oxygen_generator_rating = compute_number_from_bits(list_values_filtered[0])
    print(f"final part 2: oxygen generator rating = {oxygen_generator_rating}")

    if verbose_level >= 1:
        print(f"    [DEBUG] Finding CO2 scrubber rating...")
    least_common_bits_filtered = least_common_bits
    list_values_filtered = list_values
    for idx in range(len(most_common_bits)):
        list_values_filtered = filter_values(list_values_filtered, idx, least_common_bits_filtered[idx], verbose_level)
        assert list_values_filtered
        if len(list_values_filtered) == 1:
            if verbose_level >= 1:
                print(f"    [DEBUG] Got list with single element : {list_values_filtered}...")
            break
        most_common_bits_filtered = find_most_common_bits(list_values_filtered, verbose_level)
        least_common_bits_filtered = find_least_common_bits_from_most_common(most_common_bits_filtered)

    assert len(list_values_filtered) == 1

    print(f"final part 2: CO2 scrubber rating (raw val) = {list_values_filtered[0]}")
    co2_scrubber_rating = compute_number_from_bits(list_values_filtered[0])
    print(f"final part 2: CO2 scrubber rating = {co2_scrubber_rating}")
    print()
    print(f"final part 2: life support rating = {oxygen_generator_rating} * {co2_scrubber_rating} = {oxygen_generator_rating * co2_scrubber_rating}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
