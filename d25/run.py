import argparse
import time
import typing


class SeaCucumberMap:
    def __init__(self, list_lines: typing.List[str], verbose_level=0):
        self._nb_lines = len(list_lines)
        self._nb_cols = len(list_lines[0])
        self.verbose_level = verbose_level
        self._cucumbers_eastern = set()
        self._cucumbers_south = set()
        for y in range(self._nb_lines):
            line = list_lines[y]
            assert len(line) == self._nb_cols
            for x in range(self._nb_cols):
                val = line[x]
                if val == ">":
                    self._cucumbers_eastern.add((x, y))
                elif val == "v":
                    self._cucumbers_south.add((x, y))
                else:
                    assert val == "."

    def move_your_bodies(self) -> bool:
        someone_moved = False

        # first move eastern
        new_eastern = set()
        for cucumber in self._cucumbers_eastern:
            x, y = cucumber
            new_pos = ((x + 1) % self._nb_cols, y)
            if new_pos in self._cucumbers_eastern or new_pos in self._cucumbers_south:
                new_eastern.add(cucumber)
                continue
            new_eastern.add(new_pos)
            someone_moved = True

        # then move south
        new_south = set()
        for cucumber in self._cucumbers_south:
            x, y = cucumber
            new_pos = (x, (y + 1) % self._nb_lines)
            if new_pos in new_eastern or new_pos in self._cucumbers_south:
                new_south.add(cucumber)
                continue
            new_south.add(new_pos)
            someone_moved = True

        self._cucumbers_eastern = new_eastern
        self._cucumbers_south = new_south
        return someone_moved

    def draw_map(self):
        list_lines = [["." for _ in range(self._nb_cols)] for _ in range(self._nb_lines)]
        for set_cucumbers, char in [(self._cucumbers_eastern, ">"), (self._cucumbers_south, "v")]:
            for x, y in set_cucumbers:
                list_lines[y][x]  = char
        for line in list_lines:
            print("".join(line))


def launch_tests():
    print("Starting tests")
    map1 = SeaCucumberMap(list_lines=["...>>>>>..."])
    print("Initial state:")
    map1.draw_map()
    for step in range(1, 2 + 1):
        assert map1.move_your_bodies()
        print(f"After step {step}")
        map1.draw_map()
    map1 = SeaCucumberMap(list_lines="""
...>...
.......
......>
v.....>
......>
.......
..vvv..""".split())
    print("Initial state:")
    map1.draw_map()
    for step in range(1, 4 + 1):
        assert map1.move_your_bodies()
        print(f"After step {step}")
        map1.draw_map()

    map1 = SeaCucumberMap(list_lines="""
v...>>.vv>
.vv>>.vv..
>>.>v>...v
>>v>>.>.v.
v>v.vv.v..
>.>>..v...
.vv..>.>v.
v.v..>>v.v
....v..v.>""".split())
    print("Initial state:")
    map1.draw_map()
    for step in range(1, 57 + 1):
        assert map1.move_your_bodies()
        print(f"After step {step}...")
    for step in range(58, 58 + 1):
        assert not map1.move_your_bodies()
        print(f"After step {step}...")
        map1.draw_map()
    print("Finished tests")


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        list_lines = []
        for line in f:
            list_lines.append(line.strip())
        cucumber_map = SeaCucumberMap(list_lines, verbose_level=verbose_level)

    if not is_part1:
        raise NotImplementedError
    if verbose_level >= 2:
        print("Initial state:")
        cucumber_map.draw_map()
    step = 0
    while True:
        step += 1
        if verbose_level >= 2:
            print(f"Running step {step}...")
        if not cucumber_map.move_your_bodies():
            if verbose_level >= 1:
                print(f"FOUND not-moving cucumbers at step {step} !!")
            break
    return step


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")
    parser.add_argument("--tests", default=False, action="store_true")
    parser.add_argument("--no-run", dest="run", default=True, action="store_false")

    # parse the arguments
    args = parser.parse_args()

    if args.tests:
        launch_tests()

    # run !
    if args.run:
        res = None
        time_begin = time.time()
        try:
            res = main(args.part1, args.input, verbose_level=args.verbose)
        finally:
            time_end = time.time()
            print(f"Result = {res}")
            print(f"Computation time = {time_end - time_begin} s")
