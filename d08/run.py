import argparse
import typing


class SignalPatterns:

    _EASY_NUMBER_SEGMENT_COUNT = (2, 3, 4, 7)
    _DICT_LEN_COUNT = {2: 1, 3: 1, 4: 1, 5: 3, 6: 3, 7: 1}

    def __init__(self, line: str, verbose_level=0):
        self.verbose_level = verbose_level
        part1, part2 = line.strip().split("|")
        self._pattern_1 = part1.strip().split()
        self._pattern_2 = part2.strip().split()
        if self.verbose_level >= 3:
            print(f"[DEBUG]     Line {line.strip()} => patterns len = {len(self._pattern_1)} / {len(self._pattern_2)} ; patterns = {self._pattern_1} / {self._pattern_2}")
        assert len(self._pattern_1) == 10
        assert len(self._pattern_2) == 4

    def count_output_easy_numbers(self):
        res = 0
        for s in self._pattern_2:
            if self.verbose_level >= 2:
                print(f"[DEBUG]     signal = {s} => len = {len(s)}")
            if len(s) in self._EASY_NUMBER_SEGMENT_COUNT:
                res += 1
        return res

    def get_all_patterns_outputs(self) -> typing.List[set]:
        if self.verbose_level >= 2:
            print(f"[DEBUG]     patterns output = {self._pattern_2}")
        return [set(x for x in p) for p in self._pattern_2]

    def find_signals_from_patterns(self) -> typing.Dict[int, set]:

        if self.verbose_level >= 2:
            print(f"[DEBUG] {self._pattern_1} | {self._pattern_2}")

        all_patterns: typing.List[set] = []
        for p in self._pattern_1 + self._pattern_2:
            set_p = set(x for x in p)
            if set_p not in all_patterns:
                all_patterns.append(set_p)

        if self.verbose_level >= 2:
            print(f"[DEBUG] Found all_patterns = {all_patterns}")

        # coherency checks
        assert len(all_patterns) == 10
        patterns_by_len = {}
        for p in all_patterns:
            len_p = len(p)
            tmp = patterns_by_len.get(len_p, [])
            tmp.append(p)
            patterns_by_len[len_p] = tmp
            del tmp
        tmp_check_len = {k: len(v) for k, v in patterns_by_len.items()}
        assert tmp_check_len == self._DICT_LEN_COUNT
        del tmp_check_len

        count_signal_appearing = {}
        for s in patterns_by_len[7][0]:
            tmp_count = 0
            for p in all_patterns:
                if s in p:
                    tmp_count += 1
            count_signal_appearing[s] = tmp_count
            del tmp_count

        if self.verbose_level >= 2:
            print(f"[DEBUG] count_signal_appearing = {count_signal_appearing}")

        patterns_1 = None
        patterns_4 = None
        patterns_7 = None
        for p in all_patterns:
            len_p = len(p)
            if len_p == 2:
                patterns_1 = p
            elif len_p == 3:
                patterns_7 = p
            elif len_p == 4:
                patterns_4 = p
        if self.verbose_level >= 2:
            print(f"[DEBUG] patterns_1 = {patterns_1}")
            print(f"[DEBUG] patterns_4 = {patterns_4}")
            print(f"[DEBUG] patterns_7 = {patterns_7}")
        assert patterns_1 and patterns_4 and patterns_7

        signal_top_middle = patterns_7.difference(patterns_1)
        assert len(signal_top_middle) == 1
        signal_top_middle = next(iter(signal_top_middle))
        if self.verbose_level >= 2:
            print(f"[DEBUG] signal_top_middle   = {signal_top_middle}")

        signal_top_right = None
        signal_bottom_right = None
        for s in patterns_1:
            tmp = count_signal_appearing[s]
            if tmp == 9:
                signal_bottom_right = s
            elif tmp == 8:
                signal_top_right = s
        if self.verbose_level >= 2:
            print(f"[DEBUG] signal_top_right    = {signal_top_right}")
            print(f"[DEBUG] signal_bottom_right = {signal_bottom_right}")
        assert signal_top_right and signal_bottom_right

        signal_top_left = None
        signal_middle_middle = None
        for s in patterns_4.difference(patterns_1):
            tmp = count_signal_appearing[s]
            if tmp == 6:
                signal_top_left = s
            elif tmp == 7:
                signal_middle_middle = s
        if self.verbose_level >= 2:
            print(f"[DEBUG] signal_top_left     = {signal_top_left}")
            print(f"[DEBUG] signal_middle_middle= {signal_middle_middle}")
        assert signal_top_left and signal_middle_middle

        signal_bottom_left = None
        signal_bottom_middle = None
        for k, v in count_signal_appearing.items():
            if v == 4:
                signal_bottom_left = k
            elif v == 7 and k != signal_middle_middle:
                signal_bottom_middle = k
        if self.verbose_level >= 2:
            print(f"[DEBUG] signal_bottom_left  = {signal_bottom_left}")
            print(f"[DEBUG] signal_bottom_middle= {signal_bottom_middle}")
        assert signal_bottom_left and signal_bottom_middle

        numbers_to_signals: typing.Dict[int, set] = {
            0: set(count_signal_appearing.keys()).difference({signal_middle_middle}),
            1: {signal_top_right, signal_bottom_right},
            2: set(count_signal_appearing.keys()).difference({signal_top_left, signal_bottom_right}),
            6: set(count_signal_appearing.keys()).difference({signal_top_right}),
            8: set(count_signal_appearing.keys()),
            9: set(count_signal_appearing.keys()).difference({signal_bottom_left}),
        }
        numbers_to_signals[3] = numbers_to_signals[9].difference({signal_top_left})
        numbers_to_signals[4] = numbers_to_signals[1].union({signal_top_left, signal_middle_middle})
        numbers_to_signals[5] = numbers_to_signals[9].difference({signal_top_right})
        numbers_to_signals[7] = numbers_to_signals[1].union({signal_top_middle})

        if self.verbose_level >= 2:
            print(f"[DEBUG] numbers_to_signals = {numbers_to_signals}")

        return numbers_to_signals

    def decode_output(self) -> int:
        numbers_to_signals = self.find_signals_from_patterns()
        res = 0
        for p in self._pattern_2:
            signals_p = set(x for x in p)
            value = None
            for k, v in numbers_to_signals.items():
                if v == signals_p:
                    value = k
                    break
            assert value is not None
            res = 10 * res + value
        return res


def main(is_part1: bool, input_file: str, verbose_level=0):

    entry_list: typing.List[SignalPatterns] = []

    with open(input_file) as f:
        for line in f:
            entry_list.append(SignalPatterns(line, verbose_level=verbose_level))
            continue

    if is_part1:
        count = 0
        for entry in entry_list:
            tmp = entry.count_output_easy_numbers()
            if verbose_level >= 1:
                print(f"[DEBUG] Found count easy numbers on line = {tmp}")
            count += tmp

        print(f"Total count of easy numbers = {count}")
    else:
        count = 0
        for entry in entry_list:
            tmp = entry.decode_output()
            if verbose_level >= 1:
                print(f"[DEBUG] Found value on line = {tmp}")
            count += tmp

        print(f"Total of all output values = {count}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
