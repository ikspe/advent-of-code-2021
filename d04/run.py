import argparse
import typing


class BingoGrid:

    _NB_LINES = 5
    _NB_COLS = 5

    def __init__(self):
        self._grid = []

    def insert_new_line(self, values: typing.List[int]):
        self._grid.append([[v, 0] for v in values])

    def check_is_well_formed(self):
        assert len(self._grid) <= self._NB_LINES
        for line in self._grid:
            assert len(line) == self._NB_COLS

    @property
    def all_numbers(self) -> set:
        numbers = set()
        for line in self._grid:
            numbers.update({v[0] for v in line})
        return numbers

    @property
    def is_finished_formed(self):
        return len(self._grid) == self._NB_LINES

    def mark_number(self, num) -> bool:
        for x in range(self._NB_LINES):
            for y in range(self._NB_COLS):
                val_and_mark = self._grid[x][y]
                if val_and_mark[0] == num:
                    val_and_mark[1] = 1
                    return self.is_line_finished(x) or self.is_column_finished(y)
        assert False, f"Number {num} not found in grid !!"

    def is_line_finished(self, idx):
        line = self._grid[idx]
        for v in line:
            if not v[1]:
                return False
        return True

    def is_column_finished(self, idx):
        for y in range(self._NB_LINES):
            if not self._grid[y][idx][1]:
                return False
        return True

    def get_sum_unmarked_numbers(self):
        res = 0
        for x in range(self._NB_LINES):
            for y in range(self._NB_COLS):
                val_and_mark = self._grid[x][y]
                if val_and_mark[1] == 0:
                    res += val_and_mark[0]
        return res

    def draw(self):
        for line in self._grid:
            values = [f"{v[0]:2d}"if v[1] else "  " for v in line]
            print(" ".join(values))
        print()


class BingoGame:
    def __init__(self, list_random_numbers: typing.List[int], verbose_level=0):
        self._list_random_numbers = list_random_numbers
        self._all_grids: typing.List[BingoGrid] = []
        self._dict_values_to_grids: typing.Dict[int, typing.List[BingoGrid]] = {}
        self.verbose_level = verbose_level

    @property
    def list_random_numbers(self):
        return self._list_random_numbers

    def insert_new_grid(self, grid: BingoGrid):
        numbers_grids = grid.all_numbers
        if self.verbose_level >= 2:
            print(f"Insert new grid containing numbers {numbers_grids}")
        self._all_grids.append(grid)
        for n in numbers_grids:
            if n not in self._dict_values_to_grids:
                self._dict_values_to_grids[n] = []
            self._dict_values_to_grids[n].append(grid)

    def play(self, find_winner):
        grids_achieved = set()

        for num in self._list_random_numbers:
            if self.verbose_level >= 2:
                for grid in self._all_grids:
                    grid.draw()
            if self.verbose_level >= 1:
                print(f"Playing number : {num:2d}")
            matching_grids = self._dict_values_to_grids.get(num, [])
            for grid in matching_grids:
                is_finished = grid.mark_number(num)
                if is_finished:
                    if find_winner:
                        if self.verbose_level >= 2:
                            print(f"Grid completed :")
                            grid.draw()
                        sum_unmarked_numbers = grid.get_sum_unmarked_numbers()
                        print(f"Grid completed ! Num * sum_unmarked_numbers = {num} * {sum_unmarked_numbers} = {num * sum_unmarked_numbers}")
                        return num * sum_unmarked_numbers
                    else:
                        grids_achieved.add(grid)
                        if self.verbose_level >= 1:
                            print(f"Grid completed ! Removed from playing grids. Achieved grid = {len(grids_achieved)} / {len(self._all_grids)}")
                        if len(grids_achieved) == len(self._all_grids):
                            if self.verbose_level >= 1:
                                print(f"Last grid achieved !")
                            if self.verbose_level >= 2:
                                print(f"Grid non achieved :")
                                grid.draw()
                            sum_unmarked_numbers = grid.get_sum_unmarked_numbers()
                            print(f"Non-achieved grid ! Num * sum_unmarked_numbers = {num} * {sum_unmarked_numbers} = {num * sum_unmarked_numbers}")
                            return num * sum_unmarked_numbers


def main(is_part1: bool, input_file: str, verbose_level=0):
    current_grid: typing.Optional[BingoGrid] = None
    game = None
    with open(input_file) as f:
        is_first_line = True
        for line in f:
            if is_first_line:
                if verbose_level >= 1:
                    print("Creating new bingo game")
                game = BingoGame([int(x) for x in line.strip().split(",")], verbose_level=verbose_level)
                is_first_line = False
                continue
            line = line.strip()
            if not line:
                continue
            values = [int(x) for x in line.strip().split()]
            if not current_grid:
                if verbose_level >= 1:
                    print("    Creating bingo grid")
                current_grid = BingoGrid()
            current_grid.insert_new_line(values)
            if current_grid.is_finished_formed:
                if verbose_level >= 1:
                    print("    Finished creating bingo grid, inserting in game")
                game.insert_new_grid(current_grid)
                current_grid = None

    if verbose_level >= 1:
        print("Finished creating bingo game")
    assert game
    assert not current_grid

    game.play(find_winner=is_part1)


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
