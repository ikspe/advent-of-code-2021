import argparse
import typing


class Grid:
    def __init__(self, lines: typing.List[str], verbose_level=0):
        self._grid = [[int(x) for x in l] for l in lines]
        self._nb_lines = len(self._grid)
        self._nb_cols = len(self._grid[0])
        self.verbose_level = verbose_level

    @property
    def count(self):
        return self._nb_lines * self._nb_cols

    def _generate_adjacent_cells(self, idx_line: int, idx_col) -> typing.Set[typing.Tuple[int, int]]:
        return {(x, y)
                for x in range(max(0, idx_col - 1), min(self._nb_cols, idx_col + 2))
                for y in range(max(0, idx_line - 1), min(self._nb_lines, idx_line + 2))
                if (x, y) != (idx_col, idx_line)}

    def make_step(self):
        flashed_octopuses = set()
        points_to_be_checked = set()
        for idx_line in range(self._nb_lines):
            line = self._grid[idx_line]
            for idx_col in range(self._nb_cols):
                line[idx_col] += 1
                if line[idx_col] > 9:
                    points_to_be_checked.add((idx_col, idx_line))

        while points_to_be_checked:
            coord = points_to_be_checked.pop()
            idx_col, idx_line = coord
            if coord in flashed_octopuses:
                continue  # flash only once per step
            if self._grid[idx_line][idx_col] > 9:
                flashed_octopuses.add(coord)
                list_adjacent_cells = self._generate_adjacent_cells(idx_line=idx_line, idx_col=idx_col)
                if self.verbose_level >= 2:
                    print(f"[DEBUG] flashing = {coord} / list_adjacent_cells = {list_adjacent_cells}")
                for adj_cell in list_adjacent_cells:
                    self._grid[adj_cell[1]][adj_cell[0]] += 1
                    if self._grid[adj_cell[1]][adj_cell[0]] > 9 and adj_cell not in flashed_octopuses:
                        points_to_be_checked.add(adj_cell)

        for (idx_col, idx_line) in flashed_octopuses:
            self._grid[idx_line][idx_col] = 0
        if self.verbose_level >= 1:
            print(f"[DEBUG] flashed_octopuses = {list(flashed_octopuses)}")
        return len(flashed_octopuses)

    def draw(self):
        for line in self._grid:
            print("".join([str(x) for x in line]))


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        lines = []
        for line in f:
            lines.append(line.strip())
            continue
        grid = Grid(lines, verbose_level=verbose_level)
    if verbose_level >= 1:
        print(f"[DEBUG] Before any steps:")
        grid.draw()
    if is_part1:
        max_steps = 100
        total_flashes = 0
        for step in range(1, max_steps + 1):
            tmp_flash = grid.make_step()
            total_flashes += tmp_flash
            if verbose_level >= 1:
                print(f"[DEBUG] After step {step} : flash count = {tmp_flash} / total = {total_flashes}:")
                grid.draw()
        print(f"Total flashes = {total_flashes}")
    else:
        step = 0
        octopus_count = grid.count
        while True:
            step += 1
            tmp_flash = grid.make_step()
            if verbose_level >= 1:
                print(f"[DEBUG] After step {step} : flash count = {tmp_flash}:")
                grid.draw()
            if tmp_flash == octopus_count:
                print(f"Result: all octopuses flashing simultaneously at step = {step}")
                break


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
