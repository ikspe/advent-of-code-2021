import argparse
import typing


class ComputeFuel:

    def __init__(self, init_positions: typing.List[int], verbose_level=0):
        self._init_positions = init_positions
        self.verbose_level = verbose_level

    def display(self):
        print(f"Crab positions = {self._init_positions}")
        avg = sum(self._init_positions) / len(self._init_positions)
        print(f"Crab sum = {sum(self._init_positions)} / avg = {avg} / int(avg) = {int(avg)}")

    def find_min_position(self):
        # return int(sum(self._init_positions) / len(self._init_positions))
        return min(self._init_positions)

    def find_max_position(self):
        return max(self._init_positions)

    def compute_fuel(self, target_pos: int, linear: bool):
        res = 0
        for v in self._init_positions:
            tmp = abs(v - target_pos)
            if not linear:
                tmp = (tmp * (tmp + 1)) // 2
            if self.verbose_level >= 2:
                print(f"[DEBUG] Move from {v} to {target_pos}: {tmp} fuel")
            res += tmp
        if self.verbose_level >= 1:
            print(f"[DEBUG] Total fuel cost: {res}")
        return res


def main(is_part1: bool, input_file: str, verbose_level=0):

    with open(input_file) as f:
        compute_fuel = ComputeFuel([int(x) for x in f.readline().strip().split(",")], verbose_level=verbose_level)

    if verbose_level >= 1:
        compute_fuel.display()
    min_pos = compute_fuel.find_min_position()
    max_pos = compute_fuel.find_max_position()

    lowest_fuel = None
    best_target_pos = None
    for target_pos in range(min_pos, max_pos + 1):
        if verbose_level >= 1:
            print("========")
            print(f"[DEBUG] Computing fuel for target pos = {target_pos}")
        fuel = compute_fuel.compute_fuel(target_pos, linear=is_part1)
        if lowest_fuel is None or fuel < lowest_fuel:
            lowest_fuel = fuel
            best_target_pos = target_pos
    print(f"Found lowest fuel consumption at target pos = {best_target_pos} : fuel = {lowest_fuel}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
