import argparse
import time
import typing


class ALU:

    _REGISTER_NAMES = {"w", "x", "y", "z"}

    def __init__(self, list_instructions: typing.List[typing.List[str]]):
        self._list_instructions = list_instructions

    def execute(self, input_number: int, registers: dict = None):
        if registers is None:
            registers = {}
        assert all(k in self._REGISTER_NAMES for k in registers)
        registers = {k: registers.get(k, 0) for k in self._REGISTER_NAMES}
        input_values = [int(x) for x in str(input_number)]
        input_idx = 0
        for tokens in self._list_instructions:
            operator = tokens[0]
            operands = tokens[1:]
            assert operands[0] in self._REGISTER_NAMES
            if operator == "inp":
                assert len(operands) == 1
                registers[operands[0]] = input_values[input_idx]
                input_idx += 1
                continue
            if operator in ("add", "mul", "div", "mod", "eql"):
                assert len(operands) == 2
                val1 = operands[1]
                if val1 in self._REGISTER_NAMES:
                    val1 = registers[val1]
                else:
                    val1 = int(val1)
            else:
                val1 = None

            if operator == "add":
                registers[operands[0]] += val1
                continue
            elif operator == "mul":
                registers[operands[0]] *= val1
                continue
            elif operator == "div":
                registers[operands[0]] //= val1
                continue
            elif operator == "mod":
                registers[operands[0]] = (registers[operands[0]] % val1)
                continue
            elif operator == "eql":
                registers[operands[0]] = 1 if (registers[operands[0]] == val1) else 0
                continue
            else:
                raise ValueError(f"Unknown operator {operator}")

        return registers


def read_instructions(file_name: str) -> typing.List[typing.List[str]]:
    list_instructions = []
    with open(file_name) as f:
        for line in f:
            list_instructions.append(line.strip().split())
    return list_instructions


def read_instructions_part(part_number: int) -> typing.List[typing.List[str]]:
    assert part_number >= 0
    a, b, c = [(1, 12, 4),
               (1, 15, 11),
               (1, 11, 7),
               (26, -14, 2),
               (1, 12, 11),
               (26, -10, 13),
               (1, 11, 9),
               (1, 13, 12),
               (26, -7, 6),
               (1, 10, 2),
               (26, -2, 11),
               (26, -1, 12),
               (26, -4, 3),
               (26, -12, 13),
               ][part_number]
    lines = f"""
inp w
mul x 0
add x z
mod x 26
div z {a}
add x {b}
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y {c}
mul y x
add z y
"""
    return [it.strip().split() for it in lines.split("\n") if it.strip()]


def read_instructions_multiparts(part_first: int, part_last: int) -> typing.List[typing.List[str]]:
    instr = []
    for i in range(part_first, part_last + 1):
        instr += read_instructions_part(i)
    return instr


def solve_part1():
    """
>>> ALU(read_instructions_multiparts(1-1,3-1)).execute(999)["z"]
9324
>>> ALU(read_instructions_multiparts(1-1,4-1)).execute(9992)
{'y': 0, 'z': 358, 'w': 2, 'x': 0}
>>> alu = ALU(read_instructions_multiparts(1-1,5-1))
>>> alu.execute(99929)
{'y': 20, 'z': 9328, 'w': 9, 'x': 1}
>>> 9328%26
20
>>> alu.execute(99928)
{'y': 19, 'z': 9327, 'w': 8, 'x': 1}
>>> 9327%26
19
>>> alu = ALU(read_instructions_multiparts(1-1,6-1))
>>> alu.execute(999289)
{'y': 0, 'z': 358, 'w': 9, 'x': 0}
>>> ALU(read_instructions_multiparts(1-1,6-1)).execute(999289)
{'y': 0, 'z': 358, 'w': 9, 'x': 0}
>>> ALU(read_instructions_multiparts(1-1,7-1)).execute(9992897)
{'y': 16, 'z': 9324, 'w': 7, 'x': 1}
>>> ALU(read_instructions_multiparts(1-1,7-1)).execute(9992899)
{'y': 18, 'z': 9326, 'w': 9, 'x': 1}
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(99928999)
{'y': 21, 'z': 242497, 'w': 9, 'x': 1}
>>> 242497 % 26
21
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(99928989)
{'y': 21, 'z': 242471, 'w': 9, 'x': 1}
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(99928989)["z"]%26
21
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(99928999)["z"]%26
21
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(99928991)["z"]%26
13
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(99928998)["z"]%26
20
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(99928995)["z"]%26
17
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(99928985)["z"]%26
17
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(99928981)["z"]%26
13
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(99928991)["z"]%26
13
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(99928995)["z"]%26
17
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(99928994)["z"]%26
16
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(99928994)["z"]
242492
>>> ALU(read_instructions_multiparts(1-1,9-1)).execute(999289949)["z"]
9326
>>> ALU(read_instructions_multiparts(1-1,10-1)).execute(9992899499)["z"]
242487
>>> ALU(read_instructions_multiparts(1-1,10-1)).execute(9992899499)["z"]%26
11
>>> ALU(read_instructions_multiparts(1-1,11-1)).execute(99928994999)["z"]
9326
>>> ALU(read_instructions_multiparts(1-1,11-1)).execute(99928994999)["z"]%26
18
>>> 26 ** 4
456976
>>> 26 ** 3
17576
>>> ALU(read_instructions_multiparts(1-1,11-1)).execute(99928924999)["z"]%26
11
>>> ALU(read_instructions_multiparts(1-1,11-1)).execute(99928934999)["z"]%26
12
>>> ALU(read_instructions_multiparts(1-1,11-1)).execute(99928944999)["z"]%26
13
>>> ALU(read_instructions_multiparts(1-1,11-1)).execute(99928994999)["z"]%26
18
>>> ALU(read_instructions_multiparts(1-1,11-1)).execute(99928894999)["z"]%26
18
>>> ALU(read_instructions_multiparts(1-1,11-1)).execute(99928994999)["z"]%26
18
>>> ALU(read_instructions_multiparts(1-1,11-1)).execute(99928994999)["z"]%26
18
>>> ALU(read_instructions_multiparts(1-1,11-1)).execute(99928914999)["z"]%26
10
>>> ALU(read_instructions_multiparts(1-1,11-1)).execute(99928914999)["z"]
9318
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999289149999)["z"]
358
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999289149999)["z"]%26
20
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999288149999)["z"]%26
21
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999287149999)["z"]%26
20
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999289149999)["z"]%26
20
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999289149999)["z"]
358
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999288149999)["z"]
9329
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999287149999)["z"]
9328
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999289149999)["z"]
358
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999279149999)["z"]
9330
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999289149999)["z"]
358
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999189149999)["z"]
9311
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999289149999)["z"]
358
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(998289149999)["z"]
9312
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999289149999)["z"]
358
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(988289149999)["z"]
9286
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(999289149999)["z"]
358
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(989289149999)["z"]
357
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(929289149999)["z"]
351
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(929289149999)["z"]%26
13
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(929289149999)["z"]%26
13
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(929289149999)["z"]
351
>>> ALU(read_instructions_multiparts(1-1,13-1)).execute(9292891499999)["z"]
13
>>> ALU(read_instructions_multiparts(1-1,14-1)).execute(92928914999991)["z"]
0
    """
    return 92928914999991  # FIXME : transform the code in comment in a nice automatized algo...


def solve_part2():
    """
>>> ALU(read_instructions_multiparts(1-1,3-1)).execute(999)["z"]
9324
>>> ALU(read_instructions_multiparts(1-1,3-1)).execute(111)["z"]
3700
>>> ALU(read_instructions_multiparts(1-1,3-1)).execute(111)["z"]%26
8
>>> ALU(read_instructions_multiparts(1-1,3-1)).execute(119)["z"]%26
16
>>> ALU(read_instructions_multiparts(1-1,3-1)).execute(118)["z"]%26
15
>>> ALU(read_instructions_multiparts(1-1,3-1)).execute(118)["z"]
3707
>>> ALU(read_instructions_multiparts(1-1,4-1)).execute(1181)["z"]
142
>>> ALU(read_instructions_multiparts(1-1,5-1)).execute(11811)["z"]
3704
>>> ALU(read_instructions_multiparts(1-1,5-1)).execute(11811)["z"]%26
12
>>> ALU(read_instructions_multiparts(1-1,6-1)).execute(118112)["z"]
142
>>> ALU(read_instructions_multiparts(1-1,6-1)).execute(118112)["z"]%26
12
>>> ALU(read_instructions_multiparts(1-1,7-1)).execute(1181121)["z"]%26
10
>>> ALU(read_instructions_multiparts(1-1,7-1)).execute(1181121)["z"]
3702
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(11811211)["z"]
96265
>>> ALU(read_instructions_multiparts(1-1,8-1)).execute(11811211)["z"]%26
13
>>> ALU(read_instructions_multiparts(1-1,9-1)).execute(118112116)["z"]
3702
>>> ALU(read_instructions_multiparts(1-1,10-1)).execute(1181121161)["z"]
96255
>>> ALU(read_instructions_multiparts(1-1,10-1)).execute(1181121161)["z"]%26
3
>>> ALU(read_instructions_multiparts(1-1,11-1)).execute(11811211611)["z"]
3702
>>> ALU(read_instructions_multiparts(1-1,11-1)).execute(11811211611)["z"]%26
10
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(118112116119)["z"]
142
>>> ALU(read_instructions_multiparts(1-1,12-1)).execute(118112116119)["z"]%26
12
>>> ALU(read_instructions_multiparts(1-1,13-1)).execute(1181121161198)["z"]
5
>>> ALU(read_instructions_multiparts(1-1,13-1)).execute(1281121161198)["z"]
141
>>> ALU(read_instructions_multiparts(1-1,13-1)).execute(2181121161198)["z"]
6
>>> ALU(read_instructions_multiparts(1-1,13-1)).execute(8181121161198)["z"]
12
>>> ALU(read_instructions_multiparts(1-1,13-1)).execute(9181121161198)["z"]
13
>>> ALU(read_instructions_multiparts(1-1,14-1)).execute(91811211611981)["z"]
0
    """
    return 91811211611981  # FIXME : transform the code in comment in a nice automatized algo...


def convert_to_input_val(raw_int: int, digits=14):
    input_val = []
    for _ in range(0, digits):
        input_val.append((raw_int % 9) + 1)
        raw_int //= 9
    input_val.reverse()
    return int("".join([str(x) for x in input_val]))


def main(is_part1: bool, input_file: str, verbose_level=0):

    """
    # DON'T DO THAT : it would takes ages !!!!!!!

    alu = ALU(read_instructions(input_file))

    for raw_input_val in range(9 ** 14 - 1, -1, -1):
        input_val = convert_to_input_val(raw_input_val)
        if 3 > verbose_level >= 2:
            print(f"Executing ALU with input_val = {input_val} (raw_input_val = {raw_input_val})")
        res_alu = alu.execute(input_val)
        if verbose_level >= 3:
            print(f"Executed ALU with input_val = {input_val} (raw_input_val = {raw_input_val}), result = {res_alu}")
        if res_alu["z"] == 0:
            print(f"Found correct input : input_val = {input_val} (raw_input_val = {raw_input_val}) : res_alu = {res_alu}")
            return input_val
    """
    if is_part1:
        return solve_part1()
    else:
        return solve_part2()


def launch_tests():
    # ================
    print("Testing from very simple multiplication")
    alu = ALU([["inp", "x"], ["mul", "x", "-1"]])

    val = alu.execute(1)
    print(f"Result : {val}")
    assert val["x"] == -1

    val = alu.execute(5)
    print(f"Result : {val}")
    assert val["x"] == -5

    val = alu.execute(8)
    print(f"Result : {val}")
    assert val["x"] == -8

    # ================
    print("Testing from input-mini-example.txt...")
    alu = ALU(read_instructions("input-mini-example.txt"))

    val = alu.execute(11)
    print(f"Result : {val}")
    assert val["x"] == 1
    assert val["z"] == 0

    val = alu.execute(13)
    print(f"Result : {val}")
    assert val["x"] == 3
    assert val["z"] == 1

    val = alu.execute(31)
    print(f"Result : {val}")
    assert val["x"] == 1
    assert val["z"] == 0

    val = alu.execute(15)
    print(f"Result : {val}")
    assert val["x"] == 5
    assert val["z"] == 0

    val = alu.execute(51)
    print(f"Result : {val}")
    assert val["x"] == 1
    assert val["z"] == 0

    val = alu.execute(26)
    print(f"Result : {val}")
    assert val["x"] == 6
    assert val["z"] == 1

    val = alu.execute(62)
    print(f"Result : {val}")
    assert val["x"] == 2
    assert val["z"] == 0

    # ================
    print("Testing from input-example.txt...")
    alu = ALU(read_instructions("input-example.txt"))

    val = alu.execute(1)
    print(f"Result : {val}")
    assert val == {"w": 0, "x": 0, "y": 0, "z": 1}

    val = alu.execute(2)
    print(f"Result : {val}")
    assert val == {"w": 0, "x": 0, "y": 1, "z": 0}

    val = alu.execute(3)
    print(f"Result : {val}")
    assert val == {"w": 0, "x": 0, "y": 1, "z": 1}

    val = alu.execute(5)
    print(f"Result : {val}")
    assert val == {"w": 0, "x": 1, "y": 0, "z": 1}

    val = alu.execute(9)
    print(f"Result : {val}")
    assert val == {"w": 1, "x": 0, "y": 0, "z": 1}

    # ================
    print("Testing input values conversions")
    for raw_int, expected_val in [(0, 11111111111111),
                                  (1, 11111111111112),
                                  (1, 11111111111112),
                                  (2, 11111111111113),
                                  (3, 11111111111114),
                                  (5, 11111111111116),
                                  (6, 11111111111117),
                                  (7, 11111111111118),
                                  (8, 11111111111119),
                                  (1 * 9 ** 1, 11111111111121),
                                  (5 * 9 ** 13, 61111111111111),
                                  (5 * 9 ** 13 + 3 * 9 ** 10 + 8 * 9 ** 2 + 4, 61141111111915),
                                  ]:
        res_val = convert_to_input_val(raw_int)
        print(f"    Converting raw_int {raw_int} : result = {res_val} (expected val {expected_val})")
        assert res_val == expected_val


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")
    parser.add_argument("--tests", default=False, action="store_true")
    parser.add_argument("--no-run", dest="run", default=True, action="store_false")

    # parse the arguments
    args = parser.parse_args()

    if args.tests:
        launch_tests()

    # run !
    if args.run:
        res = None
        time_begin = time.time()
        try:
            res = main(args.part1, args.input, verbose_level=args.verbose)
        finally:
            time_end = time.time()
            print(f"Result = {res}")
            print(f"Computation time = {time_end - time_begin} s")
