import argparse
import typing


class Image:
    def __init__(self, pixels: typing.Dict[typing.Tuple[int, int], int]):
        self._pixels = pixels
        x_values = set(x for x, y in self._pixels)
        y_values = set(y for x, y in self._pixels)
        self._min_x = min(x_values)
        self._max_x = max(x_values)
        self._min_y = min(y_values)
        self._max_y = max(y_values)

    def draw(self):
        width = self._max_x - self._min_x + 1
        height = self._max_y - self._min_y + 1
        print(f"    draw() : x : {self._min_x} -> {self._max_x} / y : {self._min_y} -> {self._max_y} / width x height = {width} x {height}")
        image = [[0 for _ in range(width)] for _ in range(height)]
        for (x, y), value in self._pixels.items():
            image[y - self._min_y][x - self._min_x] = value
        for line in image:
            print("".join(["#" if x == 1 else "." for x in line]))

    def make_step(self, enhancement_algo: typing.List[int], default_pixel_val: int):
        new_pixels = {}
        # print(f"pixel  = {self._pixels}")
        for y in range(self._min_y - 1, self._max_y + 2):
            for x in range(self._min_x - 1, self._max_x + 2):
                values = []
                for i_y in range(y-1, y+2):
                    values += [self._pixels.get((i_x, i_y), default_pixel_val) for i_x in range(x-1, x+2)]
                idx_val_pixel = self._int_from_bool(values)
                # print(f"pixel ({x:3d}, {y:3d}) : idx_val_pixel = {values} = {idx_val_pixel:3d}  ==>  new pixel val =  {enhancement_algo[idx_val_pixel]}")
                new_pixels[(x, y)] = enhancement_algo[idx_val_pixel]
        # print(f"new_pixels  = {new_pixels}")
        return Image(new_pixels)

    @classmethod
    def _int_from_bool(cls, raw_bool: typing.List[int]) -> int:
        val = 0
        for x in raw_bool:
            val = val * 2 + x
        return val

    def count_pixels(self) -> int:
        return sum([v for v in self._pixels.values()])


def main(is_part1: bool, input_file: str, verbose_level=0, silent=False):
    enhancement_algo = []
    lines_image = None
    with open(input_file) as f:
        for line in f:
            line = [1 if x == "#" else 0 for x in line.strip()]
            if not line:
                if lines_image is None:
                    lines_image = []
                    continue
                raise ValueError
            if lines_image is None:
                enhancement_algo += line
            else:
                lines_image.append(line)
    assert len(enhancement_algo) == 2 ** 9
    pixels = {}
    for y in range(len(lines_image)):
        line = lines_image[y]
        for x in range(len(line)):
            pixels[(x, y)] = line[x]
    del lines_image
    image = Image(pixels)
    del pixels
    if verbose_level >= 1:
        image.draw()
    default_pixel_val = None
    if is_part1:
        number_of_steps = 2
    else:
        number_of_steps = 50
    for step in range(number_of_steps):
        if enhancement_algo[0] != 0:
            if default_pixel_val is None:
                default_pixel_val = 0
            elif default_pixel_val == 0:
                default_pixel_val = enhancement_algo[0]
            else:
                default_pixel_val = enhancement_algo[-1]
        else:
            default_pixel_val = enhancement_algo[0]
        image = image.make_step(enhancement_algo, default_pixel_val)
        if verbose_level >= 2:
            count = image.count_pixels()
            print(f"Image at step {step:3d} (number of pixels = {count}):")
            image.draw()
    if verbose_level >= 1:
        count = image.count_pixels()
        print(f"Image after {number_of_steps} step (number of pixels = {count}):")
        image.draw()
    count = image.count_pixels()
    if not silent:
        print(f"Result after {number_of_steps} steps: number of pixels = {count}")
    return count


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")
    parser.add_argument("--quiet", default=False, action="store_true")

    # parse the arguments
    args = parser.parse_args()

    # run !
    import time
    time_begin = time.time()
    res = main(args.part1, args.input, verbose_level=args.verbose, silent=args.quiet)
    time_end = time.time()
    print(f"Result: {res}")
    print(f"Total execution time = {time_end - time_begin}")
