import argparse


def main(is_part1: bool, input_file: str, verbose_level=0):
    coordinates = [0, 0]
    aim = 0
    with open(input_file) as f:
        for line in f:
            if verbose_level >= 2:
                print(f"    [DEBUG] line = {line.strip()} / coordinates = {coordinates} / aim = {aim}")
            line_splitted = line.split()
            assert len(line_splitted) == 2, line
            direction, distance = line_splitted
            distance = int(distance)
            if is_part1:
                if direction == "forward":
                    coordinates[0] += distance
                elif direction == "down":
                    coordinates[1] += distance
                elif direction == "up":
                    coordinates[1] -= distance
                else:
                    raise ValueError(f"Unexpected direction : {direction} (distance = {distance})")
            else:
                if direction == "forward":
                    coordinates[0] += distance
                    coordinates[1] += (distance * aim)
                elif direction == "down":
                    aim += distance
                elif direction == "up":
                    aim -= distance
                else:
                    raise ValueError(f"Unexpected direction : {direction} (distance = {distance})")
            if verbose_level >= 1:
                print(f"    [DEBUG] {line.strip():15} ==> coordinates = {coordinates} / aim = {aim}")

    print(f"Final coordinates = {coordinates}")

    print(f"Result = {coordinates[0] * coordinates[1]}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
