import argparse
import time
import typing


def parse_line(line: str):
    state, positions = line.split(" ", 1)
    assert state in ("on", "off")
    state = 1 if state == "on" else 0
    positions = [(axis, values.split("..")) for axis, values in [axis_values.split("=", 1) for axis_values in positions.split(",")]]
    assert len(positions) == 3
    assert [x[0] for x in positions] == ["x", "y", "z"]
    positions: typing.List[typing.Tuple[int, int]] = [tuple([int(y) for y in x[1]]) for x in positions]
    assert all([len(x) == 2 for x in positions])
    return state, positions


class Cuboid:
    def __init__(self, positions: typing.List[typing.Tuple[int, int]]):
        [self._pos_x, self._pos_y, self._pos_z] = positions

    @property
    def _positions(self):
        return self._pos_x, self._pos_y, self._pos_z

    @property
    def size(self):
        val = 1
        for axis in self._positions:
            val *= (axis[1] + 1 - axis[0])
        return val

    def has_common_values(self, other) -> bool:
        other: Cuboid = other
        for axis_idx in range(3):
            self_axis = self._positions[axis_idx]
            other_axis = other._positions[axis_idx]
            if self_axis[0] > other_axis[1]:
                return False
            if self_axis[1] < other_axis[0]:
                return False
        return True

    @classmethod
    def _extract_common_interval(cls, pos1: typing.Tuple[int, int], pos2: typing.Tuple[int, int]) -> typing.Tuple[int, int]:
        return max(pos1[0], pos2[0]), min(pos1[1], pos2[1])

    def split_from_other(self, other):
        other: Cuboid = other
        list_new_cubes: typing.List[Cuboid] = []

        if not self.has_common_values(other):
            # we could also generalize it with return [self]
            raise ValueError

        if self._pos_x[0] < other._pos_x[0] <= self._pos_x[1]:
            list_new_cubes.append(Cuboid([(self._pos_x[0], other._pos_x[0] - 1), self._pos_y, self._pos_z]))
        if self._pos_x[0] <= other._pos_x[1] < self._pos_x[1]:
            list_new_cubes.append(Cuboid([(other._pos_x[1] + 1, self._pos_x[1]), self._pos_y, self._pos_z]))
        common_pos_x = self._extract_common_interval(self._pos_x, other._pos_x)

        if self._pos_y[0] < other._pos_y[0] <= self._pos_y[1]:
            list_new_cubes.append(Cuboid([common_pos_x, (self._pos_y[0], other._pos_y[0] - 1), self._pos_z]))
        if self._pos_y[0] <= other._pos_y[1] < self._pos_y[1]:
            list_new_cubes.append(Cuboid([common_pos_x, (other._pos_y[1] + 1, self._pos_y[1]), self._pos_z]))
        common_pos_y = self._extract_common_interval(self._pos_y, other._pos_y)

        if self._pos_z[0] < other._pos_z[0] <= self._pos_z[1]:
            list_new_cubes.append(Cuboid([common_pos_x, common_pos_y, (self._pos_z[0], other._pos_z[0] - 1)]))
        if self._pos_z[0] <= other._pos_z[1] < self._pos_z[1]:
            list_new_cubes.append(Cuboid([common_pos_x, common_pos_y, (other._pos_z[1] + 1, self._pos_z[1])]))
        common_pos_z = self._extract_common_interval(self._pos_z, other._pos_z)

        list_new_cubes.append(Cuboid([common_pos_x, common_pos_y, common_pos_z]))
        return list_new_cubes

    def __eq__(self, other):
        other: Cuboid = other
        return self._positions == other._positions

    def __str__(self):
        [val_x, val_y, val_z] = [f"{axis[0]}..{axis[1]}" for axis in self._positions]
        return f"<x={val_x} y={val_y} z={val_z}>"

    def __repr__(self):
        return str(self)


def launch_tests():
    cuboid_ref = Cuboid([(-46, 7), (-6, 46), (-50, -1)])
    assert cuboid_ref == cuboid_ref
    assert cuboid_ref.has_common_values(cuboid_ref)
    list_split_ref_ref = cuboid_ref.split_from_other(cuboid_ref)
    print(f"List of split values from cuboid_ref = {cuboid_ref} with itself : len = {len(list_split_ref_ref)} / values = {list_split_ref_ref}")
    assert len(list_split_ref_ref) == 1
    assert cuboid_ref in list_split_ref_ref

    # base = (-46, 26), (-36, 17), (-47, 7), with variations on {x,y,z}_{min,max}
    for cuboid_no_match in [
                            # x doesn't match
                            Cuboid([(-49, -47), (-36, 17), (-47, 7)]),
                            Cuboid([(9, 10), (-36, 17), (-47, 7)]),

                            # y doesn't match
                            Cuboid([(-46, 26), (-36, -7), (-47, 7)]),
                            Cuboid([(-46, 26), (47, 49), (-47, 7)]),

                            # z doesn't match
                            Cuboid([(-46, 26), (-36, 17), (-100, -66)]),
                            Cuboid([(-46, 26), (-36, 17), (1, 7)]),

                            # x/y/z don't match
                            Cuboid([(-49, -47), (-36, -7), (-100, -66)]),
                            Cuboid([(9, 10), (47, 49), (1, 7)]),

                            ]:
        print(f"Comparing {cuboid_ref} with {cuboid_no_match}  (expecting no match)")
        assert cuboid_ref != cuboid_no_match
        assert cuboid_no_match != cuboid_ref
        assert not cuboid_ref.has_common_values(cuboid_no_match)
        assert not cuboid_no_match.has_common_values(cuboid_ref)

    cuboid_match = Cuboid([(-49, 1), (3, 49), (-24, 28)])
    print(f"Comparing {cuboid_ref} with {cuboid_match}  (expecting match)")
    assert cuboid_ref != cuboid_match
    assert cuboid_match != cuboid_ref
    assert cuboid_ref.has_common_values(cuboid_match)
    assert cuboid_match.has_common_values(cuboid_ref)

    list_split_cuboid_ref = cuboid_ref.split_from_other(cuboid_match)
    list_split_cuboid_match = cuboid_match.split_from_other(cuboid_ref)
    print(f"List of split values from cuboid_ref = {cuboid_ref} with cuboid_match = {cuboid_match} : len = {len(list_split_cuboid_ref)} / values = {list_split_cuboid_ref}")
    print(f"List of split values from cuboid_match = {cuboid_match} with cuboid_ref = {cuboid_ref} : len = {len(list_split_cuboid_match)} / values = {list_split_cuboid_match}")
    assert len(list_split_cuboid_ref) == 4
    assert len(list_split_cuboid_match) == 4
    assert Cuboid([(2, 7), (-6, 46), (-50, -1)]) in list_split_cuboid_ref
    assert Cuboid([(-46, 1), (-6, 2), (-50, -1)]) in list_split_cuboid_ref
    assert Cuboid([(-46, 1), (3, 46), (-50, -25)]) in list_split_cuboid_ref
    assert Cuboid([(-46, 1), (3, 46), (-24, -1)]) in list_split_cuboid_ref

    assert Cuboid([(-49, -47), (3, 49), (-24, 28)]) in list_split_cuboid_match
    assert Cuboid([(-46, 1), (47, 49), (-24, 28)]) in list_split_cuboid_match
    assert Cuboid([(-46, 1), (3, 46), (0, 28)]) in list_split_cuboid_match
    assert Cuboid([(-46, 1), (3, 46), (-24, -1)]) in list_split_cuboid_match

    print(f"All tests have passed !")


def compute_total(cuboids_state_on: typing.List[Cuboid]) -> int:
    return sum([x.size for x in cuboids_state_on])


def main(is_part1: bool, input_file: str, verbose_level=0):
    cuboids_state_on: typing.List[Cuboid] = []
    cube_values: typing.Dict[typing.Tuple[int, int, int], int] = {}

    current_line_nb = 0
    with open(input_file) as f:
        for line in f:
            state, positions = parse_line(line.strip())
            if verbose_level >= 2:
                print(f"Input (line {current_line_nb:3d}) : state = {state} / positions = {positions}")
            current_line_nb += 1

            if is_part1:
                positions = [(max(axis[0], - 50), min(axis[1], 50)) for axis in positions]
                if any([axis[0] > axis[1] for axis in positions]):
                    continue

            list_new_cuboids = [Cuboid(positions)]
            if verbose_level >= 1:
                print(f"Input (line {current_line_nb:3d}) : created cuboid : {list_new_cuboids[0]} (size {list_new_cuboids[0].size}) / for state = {state} / len(cuboids_state_on) = {len(cuboids_state_on)} / total size = {compute_total(cuboids_state_on)}")

            while list_new_cuboids:
                if verbose_level >= 3:
                    print(f"    Iterating : list_new_cuboids len = {len(list_new_cuboids)} / cuboids_state_on len = {len(cuboids_state_on)}")
                new_cuboid = list_new_cuboids.pop(0)
                if verbose_level >= 3:
                    print(f"    Iterating : new_cuboid = {new_cuboid}")

                for it_cuboid in cuboids_state_on.copy():
                    if it_cuboid == new_cuboid:
                        if state:
                            if verbose_level >= 3:
                                print(f"    Found cuboid existing in cuboids_state_on : {new_cuboid} for state = {state} => do nothing (already present)")
                            pass  # don't add a cuboid already existing
                        else:
                            if verbose_level >= 3:
                                print(f"    Found cuboid existing in cuboids_state_on : {new_cuboid} for state = {state} => remove")
                            cuboids_state_on.remove(it_cuboid)
                        new_cuboid = None
                        break
                    if not it_cuboid.has_common_values(new_cuboid):
                        continue

                    # FIXME : possible improvement :
                    # if state == 1 => do not split it_cuboid + split new_cuboid and remove common_part
                    list_split_it_cuboid = it_cuboid.split_from_other(new_cuboid)
                    list_split_new_cuboid = new_cuboid.split_from_other(it_cuboid)
                    if verbose_level >= 3:
                        print(f"    Found cuboid having common values, splitting them : new_cuboid = {new_cuboid} / it_cuboid = {it_cuboid} / split new = {list_split_new_cuboid} / split it = {list_split_it_cuboid}")
                    cuboids_state_on.remove(it_cuboid)
                    cuboids_state_on.extend(list_split_it_cuboid)
                    list_new_cuboids.extend(list_split_new_cuboid)
                    new_cuboid = None  # should not be used after that point
                    break
                if new_cuboid:
                    if state:
                        if verbose_level >= 2:
                            print(f"    Found cuboid not matching any existing cuboid : {new_cuboid} for state = {state} => added to cuboids_state_on")
                        cuboids_state_on.append(new_cuboid)
                    else:
                        if verbose_level >= 2:
                            print(f"    Found cuboid not matching any existing cuboid : {new_cuboid} for state = {state} => drop")
                # FIXME : possible improvement : try to reassemble cuboids into bigger elements to reduce fragmentation

    count = len([1 for v in cube_values.values() if v == 1])
    count2 = sum([x.size for x in cuboids_state_on])
    if verbose_level >= 1:
        print(f"Number of cubes *on* : count = {count} / count2 = {count2}")
    return count


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")
    parser.add_argument("--tests", default=False, action="store_true")
    parser.add_argument("--no-run", dest="run", default=True, action="store_false")

    # parse the arguments
    args = parser.parse_args()

    if args.tests:
        launch_tests()

    # run !
    if args.run:
        res = None
        time_begin = time.time()
        try:
            res = main(args.part1, args.input, verbose_level=args.verbose)
        finally:
            time_end = time.time()
            print(f"Result = {res}")
            print(f"Computation time = {time_end - time_begin} s")
