import argparse


def main(input_file: str, sliding_window_size: int, verbose_level=0):
    last_list_val = [None] * sliding_window_size
    last_val_sum = None
    count_larger = 0
    with open(input_file) as f:
        for val in f:
            val = int(val.strip())
            current_list_val = last_list_val[1:] + [val]
            if current_list_val[0] is None:
                current_val_sum = None
            else:
                current_val_sum = sum(current_list_val)
            if verbose_level >= 2:
                print(f"    found line val = {val} / current_list_val = {current_list_val} / current_val_sum = {current_val_sum} / last_val_sum = {last_val_sum}")
            if current_val_sum is None:
                if verbose_level >= 1:
                    print(f"{current_val_sum} (N/A - no current sum)")
            elif last_val_sum is None:
                if verbose_level >= 1:
                    print(f"{val} (N/A - no previous sum)")
            else:
                if current_val_sum > last_val_sum:
                    count_larger += 1
                    if verbose_level >= 1:
                        print(f"{val} (increased)")
                else:
                    if verbose_level >= 1:
                        print(f"{val} (decreased)")
            last_list_val = current_list_val
            last_val_sum = current_val_sum
    print(f"Count increased val = {count_larger}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("-v", "--verbose", default=0, action="count")
    parser.add_argument("--part1", dest="sliding_window_size", default=3, action="store_const", const=1)

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.input, sliding_window_size=args.sliding_window_size, verbose_level=args.verbose)
