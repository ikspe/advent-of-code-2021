import argparse
import typing


class HydrothermalVents:
    def __init__(self, allow_diags: bool, verbose_level=0):
        self._grid: typing.List[typing.List[int]] = [[0]]
        self._allow_diags = allow_diags
        self.verbose_level = verbose_level

        HydrothermalVents._debug_instance = self

    def add_line(self, x1, y1, x2, y2):
        resized = False
        min_x = min(x1, x2)
        max_x = max(x1, x2)
        if max_x >= len(self._grid[0]):
            resized = True
            added_cols = [0] * (max_x - len(self._grid[0]) + 1)
            for line in self._grid:
                line.extend(added_cols)
            if self.verbose_level >= 2:
                print(f"    [DEBUG]     adding {len(added_cols)} columns")
        min_y = min(y1, y2)
        max_y = max(y1, y2)
        if max_y >= len(self._grid):
            resized = True
            count_new_lines = 0
            for _ in range(len(self._grid), max_y + 1):
                self._grid.append([0] * len(self._grid[0]))
                count_new_lines += 1
            if self.verbose_level >= 2:
                print(f"    [DEBUG]     added {count_new_lines} lines")
            del count_new_lines
        if resized:
            if self.verbose_level >= 2:
                print(f"    [DEBUG]     new grid size : {len(self._grid[0])} x {len(self._grid)}")
        if x1 == x2:
            for y in range(min_y, max_y + 1):
                self._grid[y][x1] += 1
        elif y1 == y2:
            for x in range(min_x, max_x + 1):
                self._grid[y1][x] += 1
        else:
            if not self._allow_diags:
                if self.verbose_level >= 1:
                    print(f"    [DEBUG]         skipping line non straight (vertical or horizontal) : {x1},{y1} -> {x2},{y2}")
            else:
                if max_x - min_x != max_y - min_y:
                    raise ValueError(f"Invalid coordinates: neither straight, neither diagonal : {x1},{y1} -> {x2},{y2}")
                start_x = x1
                start_y = y1
                if x2 >= x1:
                    coeff_x = 1
                else:
                    coeff_x = -1
                if y2 >= y1:
                    coeff_y = 1
                else:
                    coeff_y = -1
                for i in range(max_x - min_x + 1):
                    self._grid[start_y + coeff_y * i][start_x + coeff_x * i] += 1

    def draw(self):
        for line in self._grid:
            print(["".join("." if x == 0 else str(x) for x in line)])

    def count(self, min_limit: int):
        res = 0
        for line in self._grid:
            for v in line:
                if v >= min_limit:
                    res += 1
        return res


def main(is_part1: bool, input_file: str, verbose_level=0):

    vents = HydrothermalVents(allow_diags=not is_part1, verbose_level=verbose_level)

    with open(input_file) as f:
        for line in f:
            tokens = line.strip().split("->")
            assert len(tokens) == 2
            coords1 = [int(x) for x in tokens[0].strip().split(",")]
            coords2 = [int(x) for x in tokens[1].strip().split(",")]
            if verbose_level >= 2:
                print(f"    [DEBUG] read line {line.strip()} => coordinates = {coords1} -> {coords2}")
            vents.add_line(*coords1, *coords2)
            continue
    if verbose_level >= 3:
        vents.draw()
    res = vents.count(2)
    print(f"Number of points where at least 2 lines overlaps = {res}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
