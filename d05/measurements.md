# Part 1

## Example

```
$ time python3 run.py --input input-example.txt --part1
Number of points where at least 2 lines overlaps = 5

real	0m0,080s
user	0m0,056s
sys	0m0,024s
```

## Real data

```
$ time python3 run.py --part1 --input ../../advent-of-code-2021-inputs/d05/input.txt
Number of points where at least 2 lines overlaps = 6856

real	0m0,151s
user	0m0,131s
sys	0m0,020s
```

# Part 2

## Example

```
$ time python3 run.py --input input-example.txt
Number of points where at least 2 lines overlaps = 12

real	0m0,079s
user	0m0,063s
sys	0m0,016s
```

## Real data

```
$ time python3 run.py --input ../../advent-of-code-2021-inputs/d05/input.txt
Number of points where at least 2 lines overlaps = 20666

real	0m0,178s
user	0m0,166s
sys	0m0,012s
```
