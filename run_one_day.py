#!/usr/bin/env python3

import argparse
import os
import subprocess
import time


def main():

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("day", default="template")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("--example", default=False, action="store_true")
    parser.add_argument("--other-input", default=None)
    parser.add_argument("-v", "--verbose", default=0, action="count")
    parser.add_argument("--measure-time", default=False, action="store_true")
    parser.add_argument("--dry-run", default=False, action="store_true")

    # parse the arguments
    args = parser.parse_args()

    if args.other_input:
        input_file = args.other_input
    elif args.example:
        input_file = "input-example.txt"
    else:
        input_file = os.path.join("..", "..", "advent-of-code-2021-inputs", args.day, "input.txt")

    verbose_args = ["-" + "v" * args.verbose] if args.verbose else []
    part_1_or_2_args = ["--part1"] if args.part1 else []

    # run !
    command_to_launch = ["python3", "run.py", *part_1_or_2_args, *verbose_args, "--input", input_file]
    if args.verbose >= 1:
        print(f"[DEBUG] run_one_day: Input file = {input_file}")
        print(f"[DEBUG] run_one_day: Executing = {command_to_launch}...")
        print(f"[DEBUG] run_one_day: Executing = {' '.join(command_to_launch)}...")
        import sys
        sys.stdout.flush()  # flushing since we run a subprocess
    if args.dry_run:
        print("DRY-RUN: stop before executing command")
        return

    time_begin = time.time()
    subprocess.check_call(command_to_launch, cwd=args.day)
    time_end = time.time()
    if args.measure_time:
        print(f"Execution time: {time_end - time_begin:.6f} seconds")


if __name__ == "__main__":
    main()
