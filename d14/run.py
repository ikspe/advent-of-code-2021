import argparse
import typing


class PolymerTemplate:
    def __init__(self, polymer: str, verbose_level=0):
        self._pair_count: typing.Dict[str, int] = {}
        self._first_elem = polymer[0]  # need to be kept
        previous_elem = self._first_elem
        for i in range(1, len(polymer)):
            current_elem = polymer[i]
            current_pair = previous_elem + current_elem
            self._pair_count[current_pair] = self._pair_count.get(current_pair, 0) + 1
            previous_elem = current_elem
        self._insertion_rules: typing.Dict[str, str] = {}
        self.verbose_level = verbose_level

    def add_rule(self, desc: str):
        str_in, str_out = tuple([x.strip() for x in desc.split("->")])
        self._insertion_rules[str_in] = str_out

    def make_step(self):
        new_pair_count: typing.Dict[str, int] = {}
        for pair, pair_count in self._pair_count.items():
            inserted = self._insertion_rules.get(pair)
            if inserted:
                for new_pair in [pair[0] + inserted, inserted + pair[1]]:
                    new_pair_count[new_pair] = new_pair_count.get(new_pair, 0) + pair_count
            else:
                new_pair_count[pair] = new_pair_count.get(pair, 0) + pair_count
        self._pair_count = new_pair_count

    def count_elements(self):
        elem_count = {self._first_elem: 1}
        for pair, count in self._pair_count.items():
            elem_count[pair[1]] = elem_count.get(pair[1], 0) + count
        return elem_count


def main(is_part1: bool, input_file: str, verbose_level=0):
    template = None
    with open(input_file) as f:
        for line in f:
            line = line.strip()
            if not template:
                template = PolymerTemplate(line, verbose_level=verbose_level)
                continue
            if not line:
                continue
            template.add_rule(line)
    assert template
    if verbose_level >= 1:
        count_elements = template.count_elements()
        print(f"Template:       len = {sum(count_elements.values())}")

    if is_part1:
        max_step = 10
    else:
        max_step = 40
    for step in range(1, max_step + 1):
        template.make_step()
        if verbose_level >= 1:
            count_elements = template.count_elements()
            print(f"After step {step:3d}: polymer len = {sum(count_elements.values())}")
    count_elements = template.count_elements()
    print(f"Count elements = {count_elements}")
    min_elem, min_count = None, None
    max_elem, max_count = None, None
    for element, count in count_elements.items():
        if min_count is None or count < min_count:
            min_elem = element
            min_count = count
        if max_count is None or count > max_count:
            max_elem = element
            max_count = count
    print(f"RESULT. Max : {(max_elem, max_count)} / Min : {(min_elem, min_count)} => Diff = {max_count - min_count}")


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    import time
    time_begin = time.time()
    main(args.part1, args.input, verbose_level=args.verbose)
    time_end = time.time()
    print(f"Measured time = {time_end - time_begin}")
