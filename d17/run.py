import argparse
import typing


class TargetArea:
    def __init__(self, target_area: str, verbose_level=0):
        assert target_area.startswith("target area:")
        target_area = target_area.removeprefix("target area:")
        target_area = [x.strip().split("=") for x in target_area.split(",")]
        assert len(target_area) == 2
        assert target_area[0][0] == "x" and target_area[1][0] == "y"
        self._target_area = [[int(t) for t in x[1].split("..", 1)] for x in target_area]
        assert all([x > 0 for x in self._target_area[0]])
        assert self._target_area[0][0] <= self._target_area[0][1]
        self.verbose_level = verbose_level

    def find_velocity(self):
        max_x = self._target_area[0][1]
        if False:
            print(f"TESTING VELOCITIES...")
            #for velocity in [[7, 2], [6, 3], [9, 0], [17, -4]]:
            for velocity in [[23,-10], [25,-9], [27,-5], [6,0], [30,-5], [24,-7]]:
                res = self.is_reaching_target(velocity.copy())
                print(f"{velocity} => res = {res}")
            print(f"...END TESTING VELOCITIES")
            return
        best_y = None
        best_velocity = None
        count_correct_velocities = 0
        for x in range(max_x + 1):
            for y in range(-200, 200):
                velocity = [x, y]
                res = self.is_reaching_target(velocity.copy())
                if self.verbose_level >= 1:
                    print(f"{velocity} => res = {res}")
                if res is not None:
                    count_correct_velocities += 1
                    if best_y is None or best_y < res[3]:
                        best_y = res[3]
                        best_velocity = velocity
        print(f"Reached {best_y} for velocity = {best_velocity} / count_correct_velocities = {count_correct_velocities}")

    def __str__(self):
        return f"<x={self._target_area[0][1]}..{self._target_area[0][1]} y={self._target_area[1][0]}..{self._target_area[1][1]}>"

    def __repr__(self):
        return str(self)

    def is_reaching_target(self, velocity) -> typing.Optional[typing.Tuple[int, int, int, int]]:
        if self.verbose_level >= 2:
            print(f"[DEBUG] trying with velocity = {velocity} / target = {self}")
        assert len(velocity) == 2
        x, y = 0, 0
        min_target_x = min(self._target_area[0])
        max_target_x = max(self._target_area[0])
        min_target_y = min(self._target_area[1])
        max_target_y = max(self._target_area[1])
        max_x = 0
        max_y = 0
        while True:
            if self.verbose_level >= 2:
                print(f"[DEBUG] now at pos = {(x, y)} with velocity = {velocity} / target = {self}")
            if min_target_x <= x <= max_target_x and min_target_y <= y <= max_target_y:
                return x, y, max_x, max_y
            if x > max_target_x:
                return None
            if velocity[0] == 0 and x < min_target_x:
                return None
            if velocity[0] == 0 and velocity[1] < 0 and y < max_target_y:
                return None
            x += velocity[0]
            y += velocity[1]
            if velocity[0] > 0:
                velocity[0] -= 1
            elif velocity[0] < 0:
                velocity[0] += 1
            velocity[1] -= 1
            max_x = max(x, max_x)
            max_y = max(y, max_y)


def main(is_part1: bool, input_file: str, verbose_level=0):
    with open(input_file) as f:
        target_area = TargetArea(f.readline().strip(), verbose_level=verbose_level)
    print(f"target = {target_area}")
    target_area.find_velocity()


if __name__ == "__main__":

    # create parser
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input", default="input-example.txt")
    parser.add_argument("--part1", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=0, action="count")

    # parse the arguments
    args = parser.parse_args()

    # run !
    main(args.part1, args.input, verbose_level=args.verbose)
